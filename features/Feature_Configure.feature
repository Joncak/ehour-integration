@FeatureConfigure
Feature: Configure eHour

@ConfigureEHour
Scenario: Create user as and Admin and Configure eHour Page
    Given Open browser and start application
    When I enter valid "admin" and "admin"
    Then I should be able see "Admin, eHour" on Welcome Page
    And I select menu item "Users"
    Then Page Header must display "User management"
    When I add a new user with the following details if non existent
	  | Username   | FirstName | LastName | Email             | Password  | ConfirmPassword | Department | UserRole 					| Active | AssignToProjectAfterSave | Country  | Location | EmployeeType |TimeSheetApprover|
	 	| JamesCam   | Jamesc     | Cameron  | Jamecam@nisum.com | yahoo$123 | yahoo$123       | Offshore  | User,Administrator	| true   | false                   | Pakistan | Karachi  | Nisum        |Admin, eHour		 |
    Then I logout from eHour
    And I am successfully logged out
    
    Given Open browser and start application
    When I enter valid "JamesCam" and "yahoo$123"
		Then I should be able see "Cameron, Jamesc" on Welcome Page
    And I should see value First_day_of_week on timesheet page
    When I select menu item "Configure eHour"
    Then I should see value of First_day_of_week on timeSheet page to be same as on configure page
    When I update First_day_of_week on Configure Page as "Tuesday"
    And I should click save button on user page
    Then I should see data saved message
    When I select menu item "Enter hours"
    Then I should see value First_day_of_week on timesheet page
    When I select menu item "Users"
	  Then Page Header must display "User management"
	  When I Select the user with username - "Cameron"
    When I press the delete button
    Then I cannot search the user with the username - "Cameron"
		When I select menu item "Configure eHour"
		Then I update First_day_of_week on Configure Page as "Sunday"
		And I should click save button on user page
    Then I should see data saved message
		
		When I logout from eHour
		Then I am successfully logged out



