package PageObjects;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.DataTable;
import stepDefinition.BaseSteps;

import java.util.concurrent.TimeUnit;

public class Page_Timesheet extends Base {

	public Page_Timesheet(String BrowserType) {
		super(BrowserType);
	}

	By by_Table_Timesheet = new By.ByXPath("//table[@class='timesheetTable']");
	By by_Table_Timesheet_Rows = new By.ByXPath(("./tbody/tr[@class='customerRows' or @Class='projectRows']"));
	By by_Table_Timesheet_Header = new By.ByXPath("./thead/tr");
	By by_Table_Timesheet_Footer = new By.ByXPath("./tfoot/tr");
	
	String xpath_Table_Timesheet_RowProject_CellProjectCode = "./td[@class='projectCode'][.//span[@class='projectCodeFilter'][text()='Ehour']]";
	By by_Table_Timesheet_RowProject_CellProjectCode;
	By by_Table_Timesheet_RowProject_CellSunday = new By.ByXPath("./td[@class='sunday']");
	By by_Table_Timesheet_RowProject_CellWeekday = new By.ByXPath("./td[@class='weekday']");
	By by_Table_Timesheet_RowProject_CellSaturday = new By.ByXPath("./td[@class='saturday']");
	By by_Heading_Timesheet = new By.ByXPath("//h3[@class='timesheetSectionHeader'][text()='Timesheet']");
	By by_Button_Store = new By.ByXPath("//a[@class='button store']");	
	By by_Message_TimesheetSaved = new By.ByXPath("//p[@class='timesheetPersisted']");
	By by_Table_Timesheet_Days = new By.ByXPath("./tr[@class='weekColumnRow']/td[@class='day']");
	By by_Table_Timesheet_Days_Name = new By.ByXPath(".//div[@class='name']");
	By by_Table_Timesheet_Days_Date = new By.ByXPath(".//div[@class='date']");
	By by_Input_Comment = new By.ByXPath("//textarea[@class='timesheetTextarea']");
	By by_DropDown_WeekLocation = new By.ByXPath("//select[@id='workLocationWeekLevel']");

	public WebElement getInputComment() throws Exception{
		return ReturnWebElement("Comments", by_Input_Comment,GlobalLongWait,GlobalShortRetry,null);
	}
	
	public WebElement getDropdownLocation() throws Exception{
		return ReturnWebElement("Dropdown - Week Level - Work Location",by_DropDown_WeekLocation,GlobalShortWait,GlobalShortRetry,null);
	}
	
	public WebElement getHeaderTimesheet() throws Exception{
		return ReturnWebElement("Heading - Label - Timesheet", by_Heading_Timesheet, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public WebElement getTableTimesheet() throws Exception{
		return ReturnWebElement("Table-Timesheet", by_Table_Timesheet, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public List<WebElement> getTableTimesheetProjectRows() throws Exception{
		return ReturnSubWebElements("Table - Timesheet - Rows", by_Table_Timesheet, by_Table_Timesheet_Rows, GlobalShortWait, GlobalShortRetry);
	}
	
	
	public WebElement getTableTimesheetFooter() throws Exception{
		return ReturnWebElement("Table - Timesheet - Footer", by_Table_Timesheet_Footer, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public WebElement getTableTimesheetCellProjectCode(String ProjectCode) throws Exception{
		by_Table_Timesheet_RowProject_CellProjectCode = new By.ByXPath(xpath_Table_Timesheet_RowProject_CellProjectCode.replace("REPLACE", ProjectCode));
		return ReturnWebElement("Table - Timesheet - Cell - ProjectCode", by_Table_Timesheet_RowProject_CellProjectCode, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public WebElement getButtonStore() throws Exception{
		return ReturnWebElement("Button - Store / Save", by_Button_Store, GlobalShortWait, GlobalShortRetry, null);
	}
	public WebElement getMessageTimesheetSaved() throws Exception{
		return ReturnWebElement("Message - Timesheet Persisted", by_Message_TimesheetSaved, GlobalShortWait, GlobalShortRetry, null);
	}

	public WebElement getTableHeader() throws Exception{
		return ReturnWebElement("Table - Header", by_Table_Timesheet_Header,GlobalShortWait, GlobalShortRetry, null);
	}

	public void SetWorkLocation(String Location) throws Exception{
	  SelectByVisibleText(getDropdownLocation(), Location);
	}
	
	public String getWorkLocation() throws Exception{
		return GetListBoxFirstSelectedElementText(getDropdownLocation());
	}
	
	By by_ProjectSpecificRowUnderAccount;

	//OLD XPATH - String xpath_ProjectSpecificRowUnderAccount = "//tr[@class='projectRow'][./td[@class='projectCode']//span[@class='projectCodeFilter'][text()='REPLACE_PROJECT_CODE']][./preceding-sibling::tr[@class='customerRow'][1][./td[@class='customerCell']//span[@class='customerName customerNameFilter'][text()='REPLACE_ACCOUNT_NAME']]]";
	String xpath_ProjectSpecificRowUnderAccount = "//tr[@class='projectRow'][./td[@class='project']//span[@class='projectName projectNameFilter'][text()='REPLACE_PRODUCT_NAME']][./preceding-sibling::tr[@class='customerRow'][1][./td[@class='customerCell']//span[@class='customerName customerNameFilter'][text()='REPLACE_ACCOUNT_NAME']]]";
	public WebElement getProjectSpecificRowUnderAccount(String AccountName, String ProjectName) throws Exception{
		by_ProjectSpecificRowUnderAccount = new By.ByXPath(xpath_ProjectSpecificRowUnderAccount.replace("REPLACE_PRODUCT_NAME", ProjectName).replace("REPLACE_ACCOUNT_NAME", AccountName));


		return ReturnWebElement("Project Specific Table Row under Account", by_ProjectSpecificRowUnderAccount, GlobalShortWait, GlobalShortRetry, null);
	}
    By by_DaySpecificXpath;
    public WebElement  getDayXpath(String Relative_Xpath) throws Exception {
        by_DaySpecificXpath = new By.ByXPath(Relative_Xpath);
        return ReturnWebElement("Project Specific Table Row under Account", by_DaySpecificXpath, GlobalShortWait, GlobalShortRetry, null);
    }

    By by_TotalTImeSheetHours = new By.ByXPath(".//*[@class='total lastColumn grandTotal']");
    public WebElement  TotalHours() throws Exception {
        return ReturnWebElement("Project Specific Table Row under Account", by_TotalTImeSheetHours, GlobalShortWait, GlobalShortRetry, null);
    }

    By by_TimesheetEntryBox;
    public WebElement  HourEntryBoxTimesheet(String ProjectNumber,String Rownumber,String Daynumber) throws Exception {
        String Temp_Xpath = "//tbody/tr[REPLACE_PROJECT_NUMBER]//*[@name='blueFrame:blueFrame_body:customers" +
                ":0" +
                ":rows" +
                ":REPLACE_PROJECTROW:days:REPLACE_DAY_NUMBER:day:day']";
        by_TimesheetEntryBox = new By.ByXPath(Temp_Xpath.replace("REPLACE_PROJECT_NUMBER",ProjectNumber).replace
                ("REPLACE_PROJECTROW",Rownumber).replace("REPLACE_DAY_NUMBER",Daynumber));
        return ReturnWebElement("Project Specific Table Row under Account", by_TimesheetEntryBox, GlobalShortWait, GlobalShortRetry, null);
    }

	//below i entered
    By by_VerifyUsernameToApproveTimesheet;
    String xpath_VerifyUsernameToApproveTimesheet = "//td[@class='customerCell']//span[@class='customerName customerNameFilter'][text()  ='REPLACE_USERNAME']";

    public WebElement verifyUsernameToApproveTimesheet(String USERNAME) throws Exception {
        by_VerifyUsernameToApproveTimesheet = new By.ByXPath(xpath_VerifyUsernameToApproveTimesheet.replace("REPLACE_USERNAME", USERNAME));
        return ReturnWebElement("Verifying Username to approve Timesheet", by_VerifyUsernameToApproveTimesheet, GlobalShortWait, GlobalShortRetry, null);
    }
	
	
	//below i added

    By by_verifyStatusCodeAfterApproval = new By.ByXPath(".//*[@class='clearFix']/div[1]/label[2]");

    public WebElement verifyStatusCodeAfterApproval() throws Exception {

        return ReturnWebElement("Verifying status code afetr timesheet approved", by_verifyStatusCodeAfterApproval, GlobalShortWait, GlobalShortRetry, null);
    }

    //below i added



    By by_verifyLabelOnApproverTimeSheet = new By.ByXPath("//td[@class='customerCell']//label");

    public WebElement verifyLabelOnApproverTimeSheet() throws Exception {
        return ReturnWebElement("Verifying label on the approver timesheet", by_verifyLabelOnApproverTimeSheet, GlobalShortWait, GlobalShortRetry, null);
    }

    //below i added

    By by_ReadingSpecificProjectName;

    public WebElement getProjectSpecificRowUnderAccount(Integer i) throws Exception {
        i = i + 2;
       // by_ReadingSpecificProjectName = new By.ByXPath("//tr[" + i.toString() + "][@class='projectRow']/td/span[@class='projectName projectNameFilter']");
        by_ReadingSpecificProjectName = new By.ByXPath("//tr[" + i.toString() + "][@class='projectRow']/td/span[@class='projectCodeFilter']");
        return ReturnWebElement("Project Specific Table Row under Account", by_ReadingSpecificProjectName, GlobalShortWait, GlobalShortRetry, null);
    }

    By by_DayBasedInputPojectName;

    public WebElement DayBasedInputPojectName(int i, int j) throws Exception {
        by_DayBasedInputPojectName = new By.ByXPath("//input[@name='blueFrame:blueFrame_body:customers:0:rows:" + i + ":days:" + j + ":day:day']");
        return ReturnWebElement("Day based project input values", by_DayBasedInputPojectName, GlobalShortWait, GlobalShortRetry, null);
    }

    public void EmptyingTimesheet() throws Exception {
        CommentsBox().clear();
        StoreTime();
    }
	
	
	
String xpath_InputBoxForDayByProjectName = "./td[contains(@class,'day')]";
	By by_Input_Pojectspecific_Hours;
	public WebElement getInputBoxForDayByProjectName(WebElement we, Integer DayNumber) throws Exception{
		String tempxpath = xpath_InputBoxForDayByProjectName + "[" + DayNumber.toString() + "]/input";
		
		by_Input_Pojectspecific_Hours = new By.ByXPath(tempxpath);
		return ReturnSubWebElement("Day Specific Input Box for Entering Hours", we, by_Input_Pojectspecific_Hours, GlobalShortWait, GlobalShortRetry);
	}
	
	By by_TimesheetTable_TimesheetRows = new By.ByXPath("./tbody/tr[@class='projectRow']");
	public List<WebElement> getAllTimesheetRows() throws Exception{
		return ReturnSubWebElements("Timesheet Rows", by_Table_Timesheet, by_TimesheetTable_TimesheetRows,GlobalShortWait, GlobalShortRetry);
	}
	
	By by_TimesheetTable_LockedTimeCells = new By.ByXPath(".//td[@class='weekday lockedCell']");
	public List<WebElement> getLockedTimeCells() throws Exception{
		return ReturnSubWebElements("Time Cells", by_Table_Timesheet, by_TimesheetTable_LockedTimeCells, GlobalShortWait, GlobalShortRetry);
	}
	
	By by_TimesheetTable_UnlockedTimeCells = new By.ByXPath("//tbody//td[contains(@class,'day')]/input[contains(@name,'blueFrame:blueFrame_body:customers')]");
	public List<WebElement> getUnlockedTimeCells() throws Exception{
		return ReturnSubWebElements("Time Cells", by_Table_Timesheet, by_TimesheetTable_UnlockedTimeCells, GlobalShortWait, GlobalShortRetry);
	}
	
	//Harish
	
	
	By by_Monday_entry = new By.ByXPath("//input[@name='blueFrame:blueFrame_body:customers:0:rows:0:days:1:day:day']");
	public WebElement MondayEntry() throws Exception{
		return driver.findElement(by_Monday_entry);
		//return ReturnWebElement("Entering the comments", by_Comments_box, GlobalShortWait, GlobalShortRetry,null);
	}
	
	
	By by_Comments_box = new By.ByXPath("//textarea[@class='timesheetTextarea']");
	public WebElement CommentsBox() throws Exception{
		return ReturnWebElement("Entering the comments", by_Comments_box, GlobalShortWait, GlobalShortRetry,null);
	}
	
	By by_Saving_timesheets = new By.ByXPath("//a[@name='submitButton']");
	public WebElement SavingTimesheets() throws Exception{
		return ReturnWebElement("Save button in timesheet page", by_Saving_timesheets, GlobalShortWait, GlobalShortRetry,null);
	}
	
	By by_Temporarily_SignIn = new By.ByXPath("//a[@class='bluebutton']/span");
	public WebElement TemporarilySignIn() throws Exception{
		return ReturnWebElement("Temporarily Sign In button", by_Temporarily_SignIn, GlobalShortWait, GlobalShortRetry,null);
	}
	
	
	public boolean VerifyingFilledHours() throws Exception {
		try {
			if(TotalFilledHours().getText().equals("63.00")) {
				return true;
			}
			Log.info("The total filled hours are not 63.00 ");
			return false;
		} catch (Exception e) {
			Log.info("The test case is failed while verifying the total hours, here is exception : "+e);
			return false;
		}
		
	}

	public boolean VerifyFilledHours(String hours, String loc) throws Exception {
		try {
			String workloc= new Select(worklocation()).getFirstSelectedOption().getText();
			BaseSteps.AssertTrue("Location Validation", loc, workloc, loc.equalsIgnoreCase(workloc));
			BaseSteps.AssertTrue("Hours Validation",hours,TotalFilledHours().getText(),hours.equalsIgnoreCase(TotalFilledHours().getText()));
			return true;
			
		} catch (Exception e) {
			Log.fatal("The test case failed while verifying the total hours and location, here is exception : "+e);
			return false;
		}
			
	}
	
	public void verifydefaultlocation(String defaultloc) throws Exception{
		By by_next_week= new By.ByXPath(".//*[@class='nextWeek']/i[@class='fa fa-angle-right']");
		ReturnWebElement("Next Week", by_next_week, GlobalShortWait, GlobalShortRetry, null).click();
		Thread.sleep(3000);
		Log.info("Verifying Default Location in the next week");
		String workloc= new Select(worklocation()).getFirstSelectedOption().getText();
		AssertTrue("Location Validation", defaultloc, workloc, true);
	}

    By by_prompt_timesheet= new By.ByXPath(".//*[@id='swal2-content']");
	public WebElement TimesheetPrompt() throws Exception {
	    return ReturnWebElement("Content in the prompt", by_prompt_timesheet, GlobalShortWait, GlobalShortRetry,null);
    }
    By by_ok_button_timesheet = new By.ByXPath("//button[@class='swal2-confirm swal2-styled'][@type='button']");
    public WebElement OkPromptTimesheet() throws Exception {
        return ReturnWebElement("Accepting the prompt in timesheet", by_ok_button_timesheet, GlobalShortWait,
                GlobalShortRetry,null);
    }

	public void VerifyTimesheetPrompt() throws Exception {
        TimesheetPrompt().getText().contains("56 hours booked for period");
        OkPromptTimesheet().click();
    }
	//This method is for temporarily signingin
	public boolean TemporarySignIn() throws Exception {

		try {
			TemporarilySignIn().click();
			
			return true;
		} catch (Exception e) {
			Log.error("The exception occured while clicking on temporarily siging in : " + e);
			return false;

		}
	}

	//Harish
	
	
//	public void FillTimeSheetData(String AccountName, DataTable table) throws Exception{
//
//		for(int i=1;i<table.raw().size();i++){
//			for(int j=1;j<table.raw().get(0).size();j++){
//				System.out.println();
//				getInputBoxForDayByProjectName(getProjectSpecificRowUnderAccount(AccountName, table.raw().get(i).get(0)), j).clear();
//				//Thread.sleep(100);
//				getInputBoxForDayByProjectName(getProjectSpecificRowUnderAccount(AccountName, table.raw().get(i).get(0)), j).sendKeys(table.raw().get(i).get(j) + "");
//				//Thread.sleep(100);
//			}
//		}
//	}
    public void FillTimeSheetData(String ProjectName) throws Exception {

        String ProjectNames[] = {"PTO", "Reserved", "shadow", "Unassigned", "Education", " Meeting&Events", "Interviews"};
        if((TotalHours().getText().equals("0.00"))||(TotalHours().getText().equals("8.00"))) {
            int Row_number = Arrays.asList(ProjectNames).indexOf(ProjectName);
            int Project_number = Row_number + 2;
            String RelativeXpath = GetProjectXpath(Project_number, Row_number);
            for (int z = 0; z < 7; z++) {
                String Temp_RelativeXpath = RelativeXpath.replace("REPLACE_DAY_NUMBER", Integer.toString(z));
                getDayXpath(Temp_RelativeXpath).clear();
                getDayXpath(Temp_RelativeXpath).sendKeys("8");
                Log.info("Filled 8 hours for the project "+ProjectName);
            }
        } else {
            ClearPreviousHours();
            int Row_number = Arrays.asList(ProjectNames).indexOf(ProjectName);
            int Project_number = Row_number + 2;
            String RelativeXpath = GetProjectXpath(Project_number, Row_number);
            for (int z = 0; z < 7; z++) {
                String Temp_RelativeXpath = RelativeXpath.replace("REPLACE_DAY_NUMBER", Integer.toString(z));
                getDayXpath(Temp_RelativeXpath).clear();
                getDayXpath(Temp_RelativeXpath).sendKeys(Keys.HOME,Keys.chord(Keys
                        .SHIFT,Keys.END),"8");
            }
        }
    }

    public void VerifyTotalHours(String TotalHours) throws Exception {
        Assert.assertTrue("The filled hours are not 56 hours!!",(TotalHours().getText()).toString()
                .equals("56.00"));
    }
    public void ClearPreviousHours() throws Exception{

        String ProjectNames[]={"PTO","Reserved","shadow","Unassigned","Education"," Meeting&Events","Interviews"};
        int Row_number = ProjectNames.length-1;
//        int Days = 7;
//        int row =
//        int Row_number= Arrays.asList(ProjectNames).indexOf(ProjectName);
//        int Project_number =Row_number+2;
//        String RelativeXpath = GetProjectXpath(Project_number,Row_number);
//        for(int z=0;z<7;z++) {
//            String Temp_RelativeXpath=RelativeXpath.replace("REPLACE_DAY_NUMBER",Integer.toString(z));
//            getDayXpath(Temp_RelativeXpath).clear();
//            getDayXpath(Temp_RelativeXpath).sendKeys("8");
//        for (int i=2;i<=Row_number+2;i++)
            for(int row =0;row<=Row_number;row++) {
                int i= row+2;
                for (int day=0;day<7;day++) {
                    String ProjectNumber = Integer.toString(i);
                    String Rownumber = Integer.toString(row);
                    String Daynumber = Integer.toString(day);
//                    HourEntryBoxTimesheet(ProjectNumber,Rownumber,Daynumber).clear();
                    if(HourEntryBoxTimesheet(ProjectNumber,Rownumber,Daynumber).equals("//tbody/tr[2]//*[@name='blueFrame:blueFrame_body:customers:0:rows:0:days:1:day:day']")) {
                        Thread.sleep(6000);
                        HourEntryBoxTimesheet(ProjectNumber,Rownumber,Daynumber).sendKeys(Keys.HOME,Keys.chord(Keys
                                .SHIFT,Keys.END),"0");
                        Thread.sleep(6000);
                    }
                    HourEntryBoxTimesheet(ProjectNumber,Rownumber,Daynumber).sendKeys(Keys.HOME,Keys.chord(Keys
                            .SHIFT,Keys.END),"0");
                    Log.info("Entered the day number :"+(day+1)+" and cleared hours for the project " +
                            ""+ProjectNames[row]);
                    }
                }

        }


//        for(int i=1;i<table.raw().size();i++){
//            for(int j=1;j<table.raw().get(0).size();j++){
//                System.out.println();
//                getInputBoxForDayByProjectName(getProjectSpecificRowUnderAccount(AccountName, table.raw().get(i).get(0)), j).clear();
//                //Thread.sleep(100);
//                getInputBoxForDayByProjectName(getProjectSpecificRowUnderAccount(AccountName, table.raw().get(i).get(0)), j).sendKeys(table.raw().get(i).get(j) + "");
//                //Thread.sleep(100);
//            }
//        }
public void ClearTimeSheetDataTable(DataTable table) throws Exception {

	for(int i=0;i<(table.raw().size()-1);i++){
		for(int j=0;j<(table.raw().get(0).size()-1);j++){
			
				DayBasedInputPojectName(i,j).clear();
			}
		}

    }
	
By by_popupok = new By.ByXPath(".//*[@id='submit']/span");
	public void StoreTime() throws Exception{
        //JavascriptExecutor js = ((JavascriptExecutor) driver);
        //js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
		getButtonStore().click();
		//Assert.assertTrue(getMessageTimesheetSaved() != null);
//		ReturnWebElement("Pop Up OK", by_popupok, GlobalLongWait, GlobalShortRetry, null).click();
	}
	
	
	
	public void FillDataInWeekdays(String ProjectCode, Integer NoOfDay){
	}

	public boolean IsTimesheetPageDisplayed() throws Exception {
		if(getHeaderTimesheet().getText().equals("Timesheet")){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean MatchCurrentWeekDaysAndDates() throws Exception {
		return true;
	}
	
	
	By by_Calendar = new By.ByXPath("//div[@class='Calendar']");
	public WebElement getCalendar() throws Exception{
		return ReturnWebElement("Calendar", by_Calendar, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_Calendar_Header = new By.ByXPath("./div[@class='CalendarHeader']");
	public WebElement getCalendarHeader() throws Exception{
		return ReturnSubWebElement("Calendar - Header", getCalendar(), by_Calendar_Header, GlobalShortWait, GlobalShortRetry);
	}
	
	String xpath_Calendar_Cell = "./div[@class='CalendarBody']//div[text()='REPLACE']";
	By by_Calendar_Cell;
	public WebElement getCalendarCell(String DateOfMonth) throws Exception{
		xpath_Calendar_Cell = xpath_Calendar_Cell.replace("REPLACE", DateOfMonth);
		by_Calendar_Cell = new By.ByXPath(xpath_Calendar_Cell);
		return ReturnSubWebElement("Calendar - Cell - Date", getCalendar(), by_Calendar_Cell,GlobalShortWait, GlobalShortRetry);
	}
	
	By by_Calendar_Header_CurrentMonth = new By.ByXPath("./div[@class='CurrentMonth']");
	public String getCalendarMonth() throws Exception{
		return (ReturnSubWebElement("Calendar - Header - Month", getCalendarHeader(), by_Calendar_Header_CurrentMonth, GlobalShortWait, GlobalShortRetry).getText());
	}
	
	By by_ClickToPreviousMonth = new By.ByXPath("//a[@title='Previous month']");
	public WebElement getLinkPreviousMonth() throws Exception{
		return ReturnWebElement("Previous Month", by_ClickToPreviousMonth, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_ClickToNextMonth = new By.ByXPath("//a[@title='Next month']");
	public WebElement getLinkNextMonth() throws Exception{
		return ReturnWebElement("Previous Month", by_ClickToNextMonth, GlobalShortWait, GlobalShortRetry, null);
	}
	
	
	public void SelectCalendarDate(Date FirstDateOfWeek) throws ParseException, Exception{
		Calendar ExpectedDateCal = Calendar.getInstance();
		ExpectedDateCal.setTime(FirstDateOfWeek);
	
		
		Calendar tempCal = Calendar.getInstance();
		tempCal.setTime(ConvertStringToDate("1 " + ExpectedDateCal.MONTH + " " + ExpectedDateCal.YEAR, "d M yyyy"));
		
		
		
		Calendar ActualDateCal = Calendar.getInstance();
		ActualDateCal.setTime(ConvertStringToDate("1 " + getCalendarMonth(),"d MMM yyyy"));
		
		while(true){
			System.out.println("Inside while");
			if(tempCal.after(ActualDateCal.getTime())){
				getLinkPreviousMonth().click();
				break;
			}else if (tempCal.before(ActualDateCal.getTime())){
				getLinkNextMonth().click();
			}else if (tempCal.equals(ActualDateCal.getTime())){
				break;
			}
		}
		
	}
	
	
	By by_Header_Week = new By.ByXPath("//h1[./a[@class='previousWeek']][./a[@class='nextWeek']]");
	public String getHeaderWeek() throws Exception{
		//System.out.println(ReturnWebElement("Header - Week", by_Header_Week, GlobalShortWait, GlobalShortRetry, null).getText().trim());
		return ReturnWebElement("Header - Week", by_Header_Week, GlobalShortWait, GlobalShortRetry, null).getText().trim();
	}
	
	public boolean CheckEnteredTimesheet(String AccountName, DataTable table) throws Exception{
		boolean flag=false;
		OuterFor:
		for(int i=1;i<table.raw().size();i++){
			InnerFor:
			for(int j=1;j<table.raw().get(0).size();j++){
		
				//Malav: This is a quick & dirty fix for the problem where selenium is not able to clear the 2nd text box of the first timesheet row. It works fine manually and I donb't see a reason it should not work with Automation but something is not correct, which is yet to be figured out.
				if(i==1 && j==2){
					continue InnerFor;
				}
				
				WebElement ProjectSpecificRow = getProjectSpecificRowUnderAccount(AccountName, table.raw().get(i).get(0));
				
				Float TableValue;
				if(table.raw().get(i).get(j).length()!=0){
					 TableValue = Float.parseFloat(table.raw().get(i).get(j) );
				}else{
					TableValue = 0f;
				}
				
				
				Float TextBoxValue;
				if(getInputBoxForDayByProjectName(ProjectSpecificRow, j).getAttribute("value").length()!=0){
					TextBoxValue = Float.parseFloat(getInputBoxForDayByProjectName(ProjectSpecificRow, j).getAttribute("value"));
				}else{
					TextBoxValue = 0f;
				}
				System.out.println("Table Value - " + TableValue);
				System.out.println("TextBox Value - " + TextBoxValue);
				System.out.println(TextBoxValue == TableValue);
				
				if(! TextBoxValue.equals(TableValue)){
					return false;
				}
				
			}
		}
		return true;
	}
	
	public void VerifyTimesheetWeekLocked(Date StartDate, Date EndDate) throws Exception{
		String DateFrom, DateTo;
		DateFrom = ConvertDateToString(StartDate, "dd MMM yyyy");
		DateTo = ConvertDateToString(EndDate, "dd MMM yyyy");
		String Temp = DateFrom + " - " + DateTo;
		Assert.assertTrue(getHeaderWeek().contains(Temp));
		
		List<WebElement> li_we_AllTimeCells = getLockedTimeCells();
		int RowCount = getAllTimesheetRows().size();
		Assert.assertTrue(li_we_AllTimeCells.size() == RowCount*7);
	}
	
	public void VerifyTimesheetWeekUnLocked(Date StartDate, Date EndDate) throws Exception{
		String DateFrom, DateTo;
		DateFrom = ConvertDateToString(StartDate, "dd MMM yyyy");
		DateTo = ConvertDateToString(EndDate, "dd MMM yyyy");
		String Temp = DateFrom + " - " + DateTo;
		Assert.assertTrue(getHeaderWeek().contains(Temp));
		
		
		List<WebElement> li_we_AllTimeCells = getUnlockedTimeCells();
		int RowCount = getAllTimesheetRows().size();
		Assert.assertTrue(li_we_AllTimeCells.size() == RowCount*7);
	}

	
	
public void fillComments(String Comments) throws Exception{
	getInputComment().clear();
	getInputComment().sendKeys(Comments);
}

public void VerifyComments(String Comments) throws Exception{
	Assert.assertTrue(getInputComment().getAttribute("value").equals(Comments));
}
	
	//Harish code 
	public void SavingTimesheet() throws Exception {
		CommentsBox().sendKeys("I am working on automation in Santiago Location");
		//SavingTimesheets().click();
		StoreTime();
		System.out.println("Stores the timesheet successfully!");
	}
	
	public void clearingPTOMondayEntry() throws Exception {
		MondayEntry().clear();
		MondayEntry().sendKeys("0.00");
	}

	By by_Switch_back = new By.ByXPath(".//div[@class='impersonatingNotification']/a[text()='switch back your own account']");
	//By by_Switch_back = new By.ByLinkText("switch back your own account");
	public WebElement SwitchBack() throws Exception{
		return ReturnWebElement("Switching back to admin account", by_Switch_back, GlobalShortWait, GlobalShortRetry, null);
	}
	public void SwitchingBackToAdmin() throws Exception {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();" , SwitchBack());
		Thread.sleep(3000);
	}
	
	By by_Verify_filled_hours = new By.ByXPath("//td[@class='total lastColumn grandTotal']");
	public WebElement TotalFilledHours() throws Exception{
		return ReturnWebElement("Total filled hours", by_Verify_filled_hours, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_work_location_this_week= new By.ByXPath(".//*[@id='workLocationWeekLevel']");
	public WebElement worklocation()throws Exception{
		return ReturnWebElement("Work Location", by_work_location_this_week, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public void selectWorkLocation(String location) throws Exception{
		String defaultloc= new Select(worklocation()).getFirstSelectedOption().getText();
		Log.info("Default Work Location of the user is "+defaultloc);
		Select oselect = new Select(worklocation());
		Thread.sleep(3000);
		oselect.selectByVisibleText(location);
		Log.info("Updated Work location of user for the given week to "+location);
		String selectedlocation=new Select(worklocation()).getFirstSelectedOption().getText();
		BaseSteps.AssertTrue("Work Location", location, selectedlocation, location.equalsIgnoreCase(selectedlocation));
	}
	
	
	

public void EditTimeSheetData(DataTable table) throws Exception{
	
	for(int i=0;i<(table.raw().size()-1);i++){
		for(int j=0;j<(table.raw().get(0).size())-1;j++){
				DayBasedInputPojectName(i,j).sendKeys(table.raw().get(i+1).get(j+1));
			}
		}
	}
public void ClearTimeSheetData(DataTable table) throws Exception{
	
	for(int i=0;i<=(table.raw().size()-1);i++){
		for(int j=0;j<(table.raw().get(0).size())-1;j++){
				DayBasedInputPojectName(i,j).clear();
				TimeUnit.SECONDS.sleep(1);
				DayBasedInputPojectName(i,j).clear();
				DayBasedInputPojectName(i,j).sendKeys(table.raw().get(i).get(j+1));
				TimeUnit.SECONDS.sleep(1);
				
			}
		}
	
	}
public static String  GetProjectXpath (int Projectnumber,int RowNumber) {
    String TempXpath= null;
    String Xpath_forSpecificDay = "//tbody/tr[REPLACE_PROJECT_NUMBER]//*[@name='blueFrame:blueFrame_body:customers" +
            ":0" +
            ":rows" +
            ":REPLACE_PROJECTROW:days:REPLACE_DAY_NUMBER:day:day']";
//    public static  getDayXpath(int Projectnumber,int RowNumber,int day) throws Exception {
////        by_DaySpecificXpath = new By.ByXpath(Xpath_forSpecificDay.replace());
//    }
    String Temp_Projectnumber= Integer.toString(Projectnumber);
    String Temp_RowNumber = Integer.toString(RowNumber);
    TempXpath = Xpath_forSpecificDay.replace("REPLACE_PROJECT_NUMBER",Temp_Projectnumber).replace("REPLACE_PROJECTROW",Temp_RowNumber);
    return TempXpath;
}
	
}
