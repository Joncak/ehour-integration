@FeatureReportFunctionality
Feature: Verifying Reports functionality
	@BCIReportvalidation
  Scenario: Verify admin is able to generate report for specific client
    Given Open browser and start application
    When I enter valid "admin" and "admin"
    Then I should be able see "Admin, eHour" on Welcome Page
    When I click menu item reporting
    Then Page shows tab as "Global report"
    Then I enter start date "8/1/2017" and end date "8/31/2017"
    When I select Clients on Global Report page
    |Client filter|
    |BCI - BCI    |
    Then I should see appropriate change in Project filter
    When I click create report button
    Then I should see tab as "Client report"
    And I should see "Report period: 8/1/17 - 8/31/17"
    Then I should see "BCI - BCI" Client on Client report
    Then I should see same selected projects on Client report
    
    
@ReportValidation
Scenario: Client Report validations
		Given Open browser and start application
    When I enter valid "admin" and "admin"
    Then I should be able see "Admin, eHour" on Welcome Page
    When I click menu item reporting
    Then Page shows tab as "Global report"
    Then I enter start date "9/01/2017" and end date "9/30/2017"
    When I select Clients on Global Report page
      |Client filter			 |
     #|!!!123 - !!!Client1 |
      |BCI - BCI    			 |
      |CS - Cencosud       |
      |gap - GAP        	 |
    And I select Users on Global Report page
    |User filter|
    |ReportJoeUser, ReportJoeUser|
    |ReportJimUser, ReportJimUser|
    |ReportLimUser, ReportLimUser|
    |ReportLeoUser, ReportLeoUser|
    |ReportJioUser, ReportJioUser|
    When I click create report button
    Then I should see tab as "Client report"
    And I should see "Report period: 9/1/17 - 9/30/17"
    Then I see the following records on generated Client report
   	
   	|Client								|Project		 				|Project Code|Role|User				 							   |Country			  |Region			 |Location		|Employee Type|Approver								 		|Timesheet Status	|Rate		|Hours|	Turnover|
   	|BCI - BCI						|BCI Consumo 				|BCI - Consumo|QA	|ReportJoeUser, ReportJoeUser|Pakistan			|Karachi		 |Karachi			|Adroit-C2C		|testmanager, testmanager		|APPROVED					|$10.00	|8		| $80.00	|
    |BCI - BCI						|BCI Consumo 				|BCI - Consumo|QA	|ReportJimUser, ReportJimUser|Pakistan			|Karachi		 |Karachi			|Nisum    		|testmanager, testmanager		|POSTED						|$10.00	|40		| $400.00	|
    |BCI - BCI						|BCI Pago de Cuentas|BCI - Pago C.|QA	|ReportLimUser, ReportLimUser|Pakistan			|Karachi		 |Karachi			|Nisum-C2C		|testmanager, testmanager	  |APPROVED					|$10.00	|16		| $160.00 |
    |gap - GAP						|PETE- ACT Sunset		|131124				|QA	|ReportLeoUser, ReportLeoUser|Pakistan			|Karachi		 |Karachi			|Adroit     	|testmanager, testmanager		|APPROVED					|$10.00	|40		| $400.00	|
    |gap - GAP						|PETE- ACT Sunset 	|131124				|QA	|ReportJioUser, ReportJioUser|Pakistan			|Karachi		 |Karachi			|Nisum Intern |testmanager, testmanager		|POSTED						|$10.00	|40		| $400.00	|		
    
    And I validate total hours "144.00" and turnover "$1,440.00" for the selected project,users combination on the report
    Then I logout from eHour
    And I am successfully logged out
    
    				
    
