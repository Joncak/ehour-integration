package stepDefinition;

import PageObjects.Log;
import PageObjects.Page_Header;
import PageObjects.Page_Login;
import PageObjects.Page_UserManagement;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.JavascriptExecutor;

import static PageObjects.Base.driver;

public class Steps_AfterLogin {

	Page_Header h = new Page_Header(null);
	Page_UserManagement pum;
	Page_Login lp = new Page_Login(null);
	
	
	@When("^I select menu item \"([^\"]*)\"$")
	public void i_select_menu_item(String MenuItem) throws Throwable {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
		h.SelectMenuItem(MenuItem);
		Log.info("Selected Menu Item : "+MenuItem);
	}
	
	@Then("^I should be able see \"([^\"]*)\" on Welcome Page$")
    public void I_should_be_able_see_on_Welcome_Page(String Name) throws Throwable {
		//lp.handleModelPopUp();
        BaseSteps.AssertTrue("User Name on Welcome Page",Name, h.getLoggedInAs(),h.getLoggedInAs().equals(Name));
        Log.info("Validated "+Name+" on the Welcome Page");
    }
	
	
	@Then("^I should be able to see \"([^\"]*)\" lastname and firstname on Welcome Page$")
	public void i_should_be_able_to_see_lastname_and_firstname_on_Welcome_Page(String uname) throws Throwable {
		String Name;
	    if(uname.equalsIgnoreCase("user1")){
	    	Name=pum.unique_lastname_u1+", "+pum.unique_firstname_u1;
	    	BaseSteps.AssertTrue("User Name on Welcome Page",Name, h.getLoggedInAs(),h.getLoggedInAs().equals(Name));
	    }else{
	    	Name=pum.unique_lastname_u2+", "+pum.unique_firstname_u2;
	    	BaseSteps.AssertTrue("User Name on Welcome Page",Name, h.getLoggedInAs(),h.getLoggedInAs().equals(Name));
	    }
	    Log.info("Validated "+uname+"'s Last and First Name on the Welcome Page");
	}



    @When("^I logout from eHour$")
	public void i_logout_from_eHour() throws Throwable {
	    h.SignOut();
	}



	@Then("^I am successfully logged out$")
	public void i_am_successfully_logged_out() throws Throwable {
	    h.VerifySignOut();
	    Log.info("Successfully Logged out of E-Hour");
	}


	//below i added
	@When("^I click on Approve Timesheet$")
	public void I_click_on_Approve_Timesheet()throws Throwable{
    h.buttonApproveTimeSheet();
	}
   //below i added
	@When("^I click on approve Anchor$")
	public void i_click_on_approve_Anchor() throws Throwable {
		h.buttonApproveAnchor();
	}
    //below i added

	@When("^I click on reject Anchor$")
	public void i_click_on_reject_Anchor() throws Throwable {
		h.buttonRejectAnchor();
	}
}
