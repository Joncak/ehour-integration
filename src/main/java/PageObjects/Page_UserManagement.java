package PageObjects;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Page_UserManagement extends Base {

	public Page_UserManagement(String BrowserType) {
		super(BrowserType);
	}
	By by_DropDown_Country = new By.ByXPath("//select[@name='countryId:user.country']");
	By by_DropDown_Location = new By.ByXPath("//select[@name='locationId:user.location']");
	By by_Dropdown_EmployeeType = new By.ByXPath("//select[@name='employeeTypeId:user.employeeType']");
	public static String unique_username_u1, unique_username_u2;
	public static String unique_lastname_u1, unique_lastname_u2;
	public static String unique_firstname_u1, unique_firstname_u2;


	By by_Timesheet_Approver=new By.ByXPath("//select[@name='user.approver']");
	public WebElement getDropDownTimesheetApprover() throws Exception{
		return ReturnWebElement("Timesheet-Approver", by_Timesheet_Approver, GlobalShortWait, GlobalShortRetry, null);
	}

	By by_Table_UserList = new ByXPath("//div[@id='listContents']/table[@class='entrySelectorTable']");
	public WebElement getTableUserList() throws Exception{
		return ReturnWebElement("Table - User List", by_Table_UserList, GlobalShortWait, GlobalShortRetry, null);
	}

	public WebElement getDropDownCountry() throws Exception{
		return ReturnWebElement("Dropdown - Country']", by_DropDown_Country, GlobalShortWait, GlobalShortRetry, null);
	}
	
	
	public WebElement getDropDownLocation() throws Exception{
		return ReturnWebElement("Dropdown - Location", by_DropDown_Location, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public WebElement getDropDownEmployeeType() throws Exception{
		return ReturnWebElement("Dropdown - Employee Type", by_Dropdown_EmployeeType, GlobalShortWait, GlobalShortRetry, null);
	}

	static By by_SaveMessage = new By.ByXPath("//span[@class='smallTextRed']");
	public WebElement getSaveMessage() throws Exception{
		return ReturnWebElement("Error / Message",by_SaveMessage,GlobalShortWait, GlobalShortRetry,null);
	}
	
	By by_Toggle_ShowInactive = new By.ByXPath(".//span[contains(@class,'fa fa-toggle-')]");
	public void ToggleInactiveFlag(Boolean ShowInactive) throws Exception{
		WebElement we = ReturnWebElement("Toggle Inactive Flag", by_Toggle_ShowInactive, GlobalShortWait,GlobalShortRetry,null);
		if(we.getAttribute("class").equals("fa fa-toggle-on") ){
			if(ShowInactive==true){
				we.click();
			}
		}
		
		if(we.getAttribute("class").equals("fa fa-toggle-off") ){
			if(ShowInactive==false){
				we.click();
			}
		}
	}
	
	String xpath_Row_DesiredEmployee = ("./tbody//tr[@class='filterRow' or @class='selectedRow'][./td[2][text()='USERNAME_REPLACE']]");
	By by_Row_DesiredEmployee;
	public WebElement getDesiredUser(String UserName) throws Exception{
		by_Row_DesiredEmployee = new By.ByXPath(xpath_Row_DesiredEmployee.replace("USERNAME_REPLACE", UserName));
		WebElement we = ReturnSubWebElement("User - Row : User Name - " + UserName, getTableOfRecords(), by_Row_DesiredEmployee, GlobalShortWait, GlobalShortRetry);
		if(we==null){
			ToggleInactiveFlag(true);
			WebElement we1 = ReturnSubWebElement("User - Row : User Name - " + UserName, getTableOfRecords(), by_Row_DesiredEmployee, GlobalShortWait, GlobalShortRetry);
			if(we1==null){
				return null;
			}
			MoveToElement(we1);
			return we1;
		}else{
		MoveToElement(we);
		return we;
		}
	}
	
	
	

	public void EditUser(List<String> DataTableRow) throws Exception{
		if(!(DataTableRow.get(1).length()==0 || GetTextBoxValue(getInputFirstName()).equals(DataTableRow.get(1)))){
			getInputFirstName().clear();
			getInputFirstName().sendKeys(DataTableRow.get(1));
		}
		
		if(!(DataTableRow.get(2).length()==0 || GetTextBoxValue(getInputLastName()).equals(DataTableRow.get(2)))){
			getInputLastName().clear();
			getInputLastName().sendKeys(DataTableRow.get(2));
		}
		

		if(!(DataTableRow.get(3).length()==0 || GetTextBoxValue(getInputEmail()).equals(DataTableRow.get(3)))){
			getInputEmail().clear();
			getInputEmail().sendKeys(DataTableRow.get(3));
		}
		
		if(!(DataTableRow.get(4).length()>0)){
			getInputPassword().sendKeys(DataTableRow.get(4));
		}
		
		if(!(DataTableRow.get(5).length()>0)){
			getInputCofirmPassword().sendKeys(DataTableRow.get(5));
		}
				
		
		if(!(DataTableRow.get(6).length()==0 || new Select(getDropDownDepartment()).getFirstSelectedOption().getText().equals(DataTableRow.get(6)))){
			new Select(getDropDownDepartment()).selectByVisibleText(DataTableRow.get(6));
		}

		
		if(!(DataTableRow.get(7).length()==0 ||VerifyListBoxSelectedValues(getListBoxUserRoles(), DataTableRow.get(7)))){
			SelectUserRoles(DataTableRow.get(7));		
		}
		
		if(!(DataTableRow.get(8).length()==0 || IsCheckBoxChecked(getCheckBoxActive()) == DataTableRow.get(8).equals("true") )){
			CheckOrUncheckTheCheckBox(DataTableRow.get(8).equals("true"), getCheckBoxActive());
		}
		
		if(!(DataTableRow.get(9).length()==0 || new Select(getDropDownCountry()).getFirstSelectedOption().getText().equals(DataTableRow.get(9)))){
			new Select(getDropDownCountry()).selectByVisibleText(DataTableRow.get(9));
		}

		if(!(DataTableRow.get(10).length()==0 || new Select(getDropDownLocation()).getFirstSelectedOption().getText().equals(DataTableRow.get(10)))){
			new Select(getDropDownLocation()).selectByVisibleText(DataTableRow.get(10));
		}
		
		if(!(DataTableRow.get(11).length()==0 || new Select(getDropDownEmployeeType()).getFirstSelectedOption().getText().equals(DataTableRow.get(11)))){
			//new Select(getDropDownEmployeeType()).selectByVisibleText(DataTableRow.get(10));
			SelectByVisibleText(getDropDownEmployeeType(),DataTableRow.get(11));
		}
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
		getButtonSave().click();
        AssertTrue("Message - Data Saved", "Data saved", getError().getText(),getError().getText().equals("Data saved"));
		getDesiredUser(DataTableRow.get(2)).click();
        
        //Verifying entered data matches the retrieved data
        

        if(!(DataTableRow.get(1).length()==0)){
            AssertTrue("TextBox - First Name", DataTableRow.get(1), GetTextBoxValue(getInputFirstName()), GetTextBoxValue(getInputFirstName()).equals(DataTableRow.get(1))); 
        }
		
		if(!(DataTableRow.get(2).length()==0)){
			AssertTrue("TextBox - Last Name", DataTableRow.get(2), GetTextBoxValue(getInputLastName()), GetTextBoxValue(getInputLastName()).equals(DataTableRow.get(2))); 
		}
		

		if(!(DataTableRow.get(3).length()==0 )){
			AssertTrue("TextBox - Email",DataTableRow.get(3),GetTextBoxValue(getInputEmail()),GetTextBoxValue(getInputEmail()).equals(DataTableRow.get(3)));
		}

		if(!(DataTableRow.get(6).length()==0 )){
			AssertTrue("ListBox - Department - Selected Value",DataTableRow.get(6),GetListBoxFirstSelectedElementText(getDropDownDepartment()),GetListBoxFirstSelectedElementText(getDropDownDepartment()).equals(DataTableRow.get(6)));
		}
        
		if(!(DataTableRow.get(7).length()==0 )){
			AssertTrue("ListBox - User Roles",DataTableRow.get(7),getListBoxUserRoles(),VerifyListBoxSelectedValues(getListBoxUserRoles(), DataTableRow.get(7)));
		}
		
		if(!(DataTableRow.get(8).length()==0 )){
			AssertTrue("Check Box - Is Active",DataTableRow.get(8).equals("true"),IsCheckBoxChecked(getCheckBoxActive()),IsCheckBoxChecked(getCheckBoxActive()) == DataTableRow.get(8).equals("true"));
		}
		
		if(!(DataTableRow.get(9).length()==0 )){
			AssertTrue("ListBox - Country - Selected Value",DataTableRow.get(9),GetListBoxFirstSelectedElementText(getDropDownCountry()),GetListBoxFirstSelectedElementText(getDropDownCountry()).equals(DataTableRow.get(9)));
		}

		if(!(DataTableRow.get(10).length()==0 )){
			AssertTrue("ListBox - Location - Selected Value",DataTableRow.get(10),GetListBoxFirstSelectedElementText(getDropDownLocation()),GetListBoxFirstSelectedElementText(getDropDownLocation()).equals(DataTableRow.get(10)));
		}

		if(!(DataTableRow.get(11).length()==0 )){
			AssertTrue("Dropdown - Employee Type - Selected Value",DataTableRow.get(11),GetListBoxFirstSelectedElementText(getDropDownEmployeeType()),GetListBoxFirstSelectedElementText(getDropDownEmployeeType()).equals(DataTableRow.get(11)));
		}
		
	}
	
	
	
	By by_ListBox_UserRoles_Selected = new By.ByXPath("./option[@selected='selected']");
	public String getSelectedUserRoles() throws Exception{
		List<WebElement> li_we_SelectedUserRoles = ReturnSubWebElements("User Roles - Selected", by_ListBox_UserRoles, by_ListBox_UserRoles_Selected, GlobalShortWait, GlobalShortRetry);
		String s = "";
		for(WebElement we:li_we_SelectedUserRoles){
			s = s + we.getText() + ",";
		}
		
		if(s.length()>0){
			s.substring(0, s.length()-2);
		}
		return s;
	}
	
	
	By by_Input_UserName = new ByXPath("//input[@name='user.username']");
	public WebElement getInputUsername() throws Exception{
		
		return ReturnWebElement("TextBox - User Name", by_Input_UserName, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_Input_FirstName = new ByXPath("//input[@name='user.firstName']");
	public WebElement getInputFirstName() throws Exception{
		return ReturnWebElement("TextBox - First Name", by_Input_FirstName, GlobalShortWait, GlobalShortRetry, null);
		
	}
	
	By by_Input_LastName = new ByXPath("//input[@name='user.lastName']");
	public WebElement getInputLastName() throws Exception{
		return ReturnWebElement("TextBox - Last Name",by_Input_LastName,GlobalShortWait,GlobalShortRetry,null);
	}
	
	By by_Input_Email = new ByXPath("//input[@name='user.email']");
	public WebElement getInputEmail() throws Exception{
		return ReturnWebElement("TextBox - Email",by_Input_Email,GlobalShortWait,GlobalShortRetry,null);
	}
	
	By by_Input_Password = new ByXPath("//input[@name='password']");
	public WebElement getInputPassword() throws Exception{
		return ReturnWebElement("TextBox - Password",by_Input_Password,GlobalShortWait,GlobalShortRetry,null);
	}
	
	By by_Input_ConfirmPassword = new ByXPath("//input[@name='confirmPassword']");
	public WebElement getInputCofirmPassword() throws Exception{
		return ReturnWebElement("TextBox - Confirm Password",by_Input_ConfirmPassword,GlobalShortWait,GlobalShortRetry,null);
	}
	By by_CheckBox_AssignProjectAfterSave = new By.ByXPath("//input[@name='showAssignments']");
	By by_CheckBox_Active = new ByXPath("//input[@name='user.active']");
	By by_ListBox_UserRoles = new ByXPath("//select[@name='user.userRoles']");
	By by_Select_FunctionalGroup = new ByXPath("//select[@name='dept:user.userDepartment']");
	public WebElement getDropDownDepartment() throws Exception{
		return ReturnWebElement("DropDownBox - Department",by_Select_FunctionalGroup,GlobalShortWait,GlobalShortRetry,null);
	}
	
	public WebElement getListBoxUserRoles() throws Exception{
		return ReturnWebElement("ListBox - User Roles",by_ListBox_UserRoles,GlobalShortWait,GlobalShortRetry,null);
	}
	
	public WebElement getCheckBoxActive() throws Exception{
		return ReturnWebElement("CheckBox - Active",by_CheckBox_Active,GlobalShortWait,GlobalShortRetry,null);
	}
	
	
	public WebElement getCheckBoxAssignProjectAfterSave() throws Exception{
		return ReturnWebElement("CheckBox - Assign Project After Save",by_CheckBox_AssignProjectAfterSave,GlobalShortWait, GlobalShortRetry,null);
	}
	
	By by_Button_Save = new By.ByName("submitButton");
	public WebElement getButtonSave() throws Exception{
		return ReturnWebElement("Button - Save",by_Button_Save,GlobalShortWait, GlobalShortRetry,null);
	    //return driver.findElement(by_Button_Save);
	}
	
	By by_Error = new By.ByXPath("//span[@class='formValidationError']");
	public WebElement getError() throws Exception{
		return ReturnWebElement("Error / Message",by_Error,GlobalShortWait, GlobalShortRetry,null);
	}
	
	By by_Input_Filter = new By.ById("listFilter");
	public WebElement getInputFilter() throws Exception{
		return ReturnWebElement("TextBox - Filter",by_Input_Filter,GlobalShortWait, GlobalShortRetry,null);
	}
	
	public void SelectUserRoles(String UserRolesCommaSeparated) throws Exception{
		new Select(getListBoxUserRoles()).deselectAll();
		String[] arrUserRoles = UserRolesCommaSeparated.split(",");
		for(String s:arrUserRoles){
			new Select(getListBoxUserRoles()).selectByVisibleText(s);
			getListBoxUserRoles().sendKeys(Keys.CONTROL);
		}
	}
	
	
	By by_ButtonDelete = new By.ByXPath("//a[@class='button'][./span[text()='Delete']]");
	public WebElement getButtonDelete() throws Exception{
		return ReturnWebElement("Button - Delete",by_ButtonDelete,GlobalShortWait,GlobalShortRetry,null);
	}

	
	//Harish
	By by_displayedProject = new ByXPath("//body/div[2]/div/div/div/div/div/div/div[2]/div/form/div[2]/div/div[2]/table/tbody/tr[2]/td[2]");
	public WebElement displayedProject() throws Exception{
		
		return ReturnWebElement("Displayed Project", by_displayedProject, GlobalShortWait, GlobalShortRetry, null);
	}

	By by_Select_project = new ByXPath("//body/div[2]/div/div[2]/div/div[2]/div/div/div/div/form/table/tbody/tr/td[2]/select[@name='formComponents:projectSelection:customer']");
	public WebElement SelectProject() throws Exception{
	return ReturnWebElement("Select Project", by_Select_project, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_Select_streaminProject = new ByXPath("//body/div[2]/div/div[2]/div/div[2]/div/div/div/div/form/table/tbody/tr[2]/td[2]/select/optgroup/option");
	public WebElement SelectstreaminProject() throws Exception{
		return ReturnWebElement("Select Stream In Project", by_Select_streaminProject, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_startdate_checkbox = new ByXPath("//body/div[2]/div/div[2]/div/div[2]/div/div/div/div/form/table/tbody/tr[9]/td[2]/span/input");
	public WebElement getstartcheckboxstatus() throws Exception{
		
		return ReturnWebElement("Start Date Check Box", by_startdate_checkbox, GlobalShortWait, GlobalShortRetry, null);
	}
	By by_enddate_checkbox = new ByXPath("//body/div[2]/div/div[2]/div/div[2]/div/div/div/div/form/table/tbody/tr[10]/td[2]/span/input");
	public WebElement getendcheckboxstatus() throws Exception{
		
		return ReturnWebElement("End Date Check Box", by_enddate_checkbox, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_start_date = new ByXPath("//body/div[2]/div/div[2]/div/div[2]/div/div/div/div/form/table/tbody/tr[9]/td[2]/span/input");
	public WebElement getstartDate() throws Exception{
		
		return ReturnWebElement("Start Date", by_start_date, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_Select_Role = new ByXPath("//*[@name='formComponents:rateRole:projectAssignment.role']");
	public WebElement SelectroleinProject() throws Exception{
		
		return ReturnWebElement("Select Role", by_Select_Role, GlobalShortWait, GlobalShortRetry, null);
	}
	
	
	By by_Select_HourlyRate = new ByXPath("//body/div[2]/div/div[2]/div/div[2]/div/div/div/div/form/table/tbody/tr[13]/td[2]/input");
	public WebElement SelectHourlyRate() throws Exception{
		
		return ReturnWebElement("Select Hourly Rate", by_Select_HourlyRate, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_Select_AssignmentType = new ByXPath("//body/div[2]/div/div[2]/div/div[2]/div/div/div/div/form/table/tbody/tr[4]/td[2]/select");
	public WebElement SelectAssignmentType() throws Exception{
		
		return ReturnWebElement("Select Assignment Type", by_Select_AssignmentType, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_Saving_AssignedProject = new ByXPath("//body/div[2]/div/div[2]/div/div[2]/div/div/div/div/form/table/tbody/tr[17]/td[2]/a[@name='submitButton']");
	public WebElement SavingAssignedproject() throws Exception{
		
		return ReturnWebElement("Save Assigned Project", by_Saving_AssignedProject, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_end_date = new ByXPath("//body/div[2]/div/div[2]/div/div[2]/div/div/div/div/form/table/tbody/tr[10]/td[2]/span/input");
	public WebElement getendDate() throws Exception{
		
		return ReturnWebElement("End Date", by_end_date, GlobalShortWait, GlobalShortRetry, null);
	}
	
	
	By by_logOut = new ByXPath("//body/div/div[2]/div/a[2]");
	public WebElement LogOut() throws Exception{
		
		return ReturnWebElement("Logout Button", by_logOut, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_previous_month = new ByXPath("//a[@title='Previous month']");
	public WebElement previous_month() throws Exception{
		return ReturnWebElement("Previous Month", by_previous_month, GlobalShortWait, GlobalShortRetry, null);
	}
	
	//By week_in_previous_month = new ByXPath("//div[@class='CalendarWeek other LastWeek']");
	By week_in_previous_month = new ByXPath("html/body/div[2]/div/div[2]/div[1]/div/div[2]/div[2]/div[5]/div[7]");
	public WebElement week_in_previous_month() throws Exception{	
		//return driver.findElement(week_in_previous_month);
		return ReturnWebElement("Week_In_Previous_Month", week_in_previous_month, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_next_month = new ByXPath("//a[@title='Next month']");
	public WebElement next_month() throws Exception{
		//return driver.findElement(by_next_month);
		return ReturnWebElement("Next_Month", by_next_month, GlobalShortWait, GlobalShortRetry, null);
	}
	
	//By week_in_after_month = new ByXPath("//div[@class='CalendarWeek other LastWeek']");
	By week_in_after_month = new ByXPath("html/body/div[2]/div/div[2]/div[1]/div/div[2]/div[2]/div[3]/div[6]");
	public WebElement week_in_after_month() throws Exception{
		//return driver.findElement(week_in_after_month);
		return ReturnWebElement("Week_In_After_Month", week_in_after_month, GlobalShortWait, GlobalShortRetry, null);
	}
	
	By by_next_week= new By.ByXPath(".//*[@class='nextWeek']/i[@class='fa fa-angle-right']");
	public WebElement next_week() throws Exception{
		return ReturnWebElement("Next Week", by_next_week, GlobalShortWait, GlobalShortRetry, null);
	}
	//Harish
	
	public void CreateUser(List<String> DataTableRow) throws Exception{
		String unique_username=DataTableRow.get(0)+Base.GetDateTimeInYYYYMMDDHHMMSS();
		String unique_firstname=DataTableRow.get(1)+Base.GetDateTimeInYYYYMMDDHHMMSS();
		String unique_lastname=DataTableRow.get(2)+Base.GetDateTimeInYYYYMMDDHHMMSS();
		
		this.unique_username_u1=unique_username;
		this.unique_firstname_u1=unique_firstname;
		this.unique_lastname_u1=unique_lastname;
		
		getInputUsername().sendKeys(this.unique_username_u1);
		getInputFirstName().sendKeys(this.unique_firstname_u1);
		getInputLastName().sendKeys(this.unique_lastname_u1);
		getInputEmail().sendKeys(DataTableRow.get(3));
		getInputPassword().sendKeys(DataTableRow.get(4));
		getInputCofirmPassword().sendKeys(DataTableRow.get(5));
		SelectByVisibleText(getDropDownDepartment(),DataTableRow.get(6));
		
		SelectUserRoles(DataTableRow.get(7));
		CheckOrUncheckTheCheckBox(DataTableRow.get(8).equals("true"), getCheckBoxActive());
		CheckOrUncheckTheCheckBox(DataTableRow.get(9).equals("true"), getCheckBoxAssignProjectAfterSave());
		SelectByVisibleText(getDropDownCountry(), DataTableRow.get(10));
		SelectByVisibleText(getDropDownLocation(),DataTableRow.get(11));
		SelectByVisibleText(getDropDownEmployeeType(),DataTableRow.get(12));
		SelectByVisibleText(getDropDownTimesheetApprover(),DataTableRow.get(13));
		getButtonSave().click();
		
		if(DataTableRow.get(9).equals("false")){
        AssertTrue("Error - Data Saved","Data saved", getError().getText(),getError().getText().equals("Data saved"));
		}
        
	}
	
	public void CreateUser2(List<String> DataTableRow) throws Exception{
		String unique_username=DataTableRow.get(0)+Base.GetDateTimeInYYYYMMDDHHMMSS();
		String unique_firstname=DataTableRow.get(1)+Base.GetDateTimeInYYYYMMDDHHMMSS();
		String unique_lastname=DataTableRow.get(2)+Base.GetDateTimeInYYYYMMDDHHMMSS();
		
		this.unique_username_u2=unique_username;
		this.unique_firstname_u2=unique_firstname;
		this.unique_lastname_u2=unique_lastname;
		
		getInputUsername().sendKeys(this.unique_username_u2);
		getInputFirstName().sendKeys(this.unique_firstname_u2);
		getInputLastName().sendKeys(this.unique_lastname_u2);
		
		getInputEmail().sendKeys(DataTableRow.get(3));
		getInputPassword().sendKeys(DataTableRow.get(4));
		getInputCofirmPassword().sendKeys(DataTableRow.get(5));
		SelectByVisibleText(getDropDownDepartment(),DataTableRow.get(6));
		
		SelectUserRoles(DataTableRow.get(7));
		CheckOrUncheckTheCheckBox(DataTableRow.get(8).equals("true"), getCheckBoxActive());
		CheckOrUncheckTheCheckBox(DataTableRow.get(9).equals("true"), getCheckBoxAssignProjectAfterSave());
		SelectByVisibleText(getDropDownCountry(), DataTableRow.get(10));
		SelectByVisibleText(getDropDownLocation(),DataTableRow.get(11));
		SelectByVisibleText(getDropDownEmployeeType(),DataTableRow.get(12));
		SelectByVisibleText(getDropDownTimesheetApprover(),unique_lastname_u1+", "+unique_firstname_u1);
		
		getButtonSave().click();
        //AssertTrue("Error - Data Saved","Data saved", getError().getText(),getError().getText().equals("Data saved"));
	}
	
	public void CreateUserAssignProject(List<String> DataTableRow) throws Exception{
       // driver.switchTo().findElement(By.class("GreyFrameBody")));
        //driver.switchTo().frame(0);
       // driver.switchTo(driver.findElement(By.cssSelector(".GreyFrameContainer > div:nth-child(2)")));
//        driver.switchTo().frame(".GreyFrameBody");
		getInputUsername().sendKeys(DataTableRow.get(0));
		getInputFirstName().sendKeys(DataTableRow.get(1));
		getInputLastName().sendKeys(DataTableRow.get(2));
		getInputEmail().sendKeys(DataTableRow.get(3));
		getInputPassword().sendKeys(DataTableRow.get(4));
		getInputCofirmPassword().sendKeys(DataTableRow.get(5));
		SelectByVisibleText(getDropDownDepartment(),DataTableRow.get(6));

		SelectUserRoles(DataTableRow.get(7));
		CheckOrUncheckTheCheckBox(DataTableRow.get(8).equals("true"), getCheckBoxActive());
		CheckOrUncheckTheCheckBox(DataTableRow.get(9).equals("true"), getCheckBoxAssignProjectAfterSave());
		Thread.sleep(2000);
		SelectByVisibleText(getDropDownCountry(), DataTableRow.get(10));
		Thread.sleep(2000);
		SelectByVisibleText(getDropDownLocation(),DataTableRow.get(11));
        //Robot robot = new Robot();
        //robot.keyPress(KeyEvent.VK_PAGE_DOWN);
        //robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
		SelectByVisibleText(getDropDownEmployeeType(),DataTableRow.get(12));
		SelectByVisibleText(getDropDownTimesheetApprover(),DataTableRow.get(13));
		getButtonSave().click();
		Thread.sleep(4000);
	}

		public void DeleteUser(String UserName) throws Exception{
			if(getDesiredUser(UserName)!=null){
				getDesiredUser(UserName).click();
                JavascriptExecutor js = ((JavascriptExecutor) driver);
                js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
				getButtonDelete().click();
				getDeleteAlert().accept();
				Thread.sleep(5000);
				AssertTrue("User Row", "NULL", getDesiredUser(UserName)!=null?"NOT NULL":"NULL", getDesiredUser(UserName)==null);
			}
		}
	
		public void PressDeleteButton() throws Exception{
            JavascriptExecutor js = ((JavascriptExecutor) driver);
            js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
			getButtonDelete().click();
			getDeleteAlert().accept();
			Log.info("Clicked on Delete Button");
            Thread.sleep(5000);

		}
	
	public int getUserSearchResultCount(String UserName) throws Exception{
		getInputFilter().clear();
		getInputFilter().sendKeys(UserName);
		
		List<String> ListOfUsers = getListOfUsers();
		
		if(ListOfUsers==null){
			return 0;
		}else{
			return ListOfUsers.size();
		}
	}
	By by_errormessage_dates = new By.ByXPath("//table[@class='contentTable']/tbody/tr[9]/td[2]/span/span/div");
	public WebElement getDateError() throws Exception {
		return ReturnWebElement("Start date can't be after end date", by_errormessage_dates, GlobalShortWait, GlobalShortRetry, null);
	}
	public WebElement getUserRowFromSearchResultsByUserName(String UserName) throws Exception{
		//return ReturnWebElement("User Row From Search Results", by_SearchResults_UserRow, GlobalShortWait, GlobalShortRetry, null);
		return getTableUserList().findElement(By.xpath(".//tbody/tr[@class='filterRow' and ./td[text()='"+ UserName +"'] and (not(@style) or @style='display: table-row;')]"));
	}
	
	public void SelectUserRowFromSearchResults(String UserName) throws Exception{
		getUserRowFromSearchResultsByUserName(UserName).click();
		//Thread.sleep(2000);
		System.out.println("Input Attribute Value - " + getInputUsername().getAttribute("value"));
		Assert.assertTrue(getInputUsername().getAttribute("value").equals(UserName));
	}
	
	//Harish
	
public boolean Verify_start_end_date() throws InterruptedException {
		
		
		if(driver.getPageSource().contains("Scotiabank")/*displayedProject().getText().contains("BCI Autenticacion")*/) {
			//Thread.sleep(3000);
			System.out.println("!!!!!!!!!!!!!!!!!!!!!");
			return true;
		} else {
			return false;
		}
		
	}


private static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
public void SetDate(List<String> DataTableRow) throws Exception{
	
	Select dropdown = new Select(SelectProject());
	dropdown.selectByVisibleText("SB - Scotiabank");
	//Thread.sleep(5000);
	SelectstreaminProject().click();
	//Thread.sleep(5000);
	Select AssignmentTypedropdown = new Select(SelectAssignmentType());
	AssignmentTypedropdown.selectByVisibleText(DataTableRow.get(0));

	if(getstartcheckboxstatus().isSelected()){
		Log.info("Start date checkbox is in selected state");
		getstartcheckboxstatus().click();
	} else {
		Log.info("Start date checkbox is already unselected");
	}
	//Thread.sleep(10000);
	if(getendcheckboxstatus().isSelected()) {
		Log.info("End date checkbox is in selected state");
		getendcheckboxstatus().click();
	} else {
		Log.info("End date checkbox is already unselected");
	}



	Date currentDate = new Date();
	getstartDate().sendKeys(dateFormat.format(currentDate));
	//Thread.sleep(3000);
    
    // convert date to calendar
    Calendar c = Calendar.getInstance();
    c.setTime(currentDate);

    // manipulate date
    c.add(Calendar.YEAR, 0);
    c.add(Calendar.MONTH, 1);
    c.add(Calendar.DATE, 0); //same with c.add(Calendar.DAY_OF_MONTH, 1);
    c.add(Calendar.HOUR, 0);
    c.add(Calendar.MINUTE, 0);
    c.add(Calendar.SECOND, 0);

    // convert calendar to date
    Date currentDatePlusOne = c.getTime();
    getendDate().sendKeys(dateFormat.format(currentDatePlusOne));
    //Thread.sleep(3000);
	

	SelectroleinProject().sendKeys("QA Engineer");
	SelectHourlyRate().sendKeys("60");
	SavingAssignedproject().click();
	LogOut().click();
	//Thread.sleep(10000);
}

/*public boolean check_project_in_other_dates() throws Exception {
	int i=0;
	try {
		previous_month().click();
		System.out.println("Navigating to the previous month");
		//SelectingWeek().click();
		if(!(displayedProject().getText().contains("BCI Cells"))) {
			i++;
			for(int m=1;m<3;m++) {
				System.out.println("Setting the date to "+m+" month later");
				next_month().click();
				if(m==2) {
					SelectingWeek().click();
				}
			}
			
			if(!(displayedProject().getText().contains("BCI Cells"))) {
				i++;
			} 
		} 
		
		if(i==2) {
			LogOut().click();
			return true;
		} else {
			LogOut().click();
			return false;
		}
	} catch (Exception e) {
		System.out.println(e);
		return false;
	}
	
}*/
	

public boolean check_project_in_other_dates() throws Exception {
	int i=0;
	try {
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by_previous_month));
		previous_month().click();
		Log.info("Navigating to the previous month");
		Thread.sleep(4000);
		week_in_previous_month().click();
		Log.info("Selecting week in previous month");
		Thread.sleep(4000);
		if(driver.getPageSource().contains("Scotiabank")) {
			Log.info("Project is displayed in the dates other than the assigned dates!!");
		} 
		else {
			i++;
			wait.until(ExpectedConditions.visibilityOfElementLocated(by_next_month));
			for(int m=1;m<3;m++) {
				Log.info("Setting the date to "+m+" month later");
				Thread.sleep(4000);
				Log.info("QQQQ"+m);
				next_month().click();
				if(m==2) {
					//SelectweekinAFterMonth().click();
					//week_in_after_month().click();
					Thread.sleep(4000);
					next_week().click();
					Log.info("Selecting week in after month");
				}
			}
			Thread.sleep(5000);
			if(driver.getPageSource().contains("Scotiabank")) {
				Log.warn("Project is displayed in the dates other than the assigned dates!!!");
			} else {
				i++;
			}
		}
		Thread.sleep(5000);
		if(i==2) {
			LogOut().click();
			return true;
		} else {
			return false;
		}
	} catch (Exception e) {
		Log.error("The exception is :"+e);
		return false;
	}
	
}



	//Harish

	By by_Week_in_aftermonth = new By.ByXPath("//div[@class='CalendarWeek other LastWeek']");
	public WebElement SelectweekinAFterMonth() throws Exception{
		return ReturnWebElement("Week selection",by_Week_in_aftermonth,GlobalShortWait, GlobalShortRetry, null);
	}
	
	
	By by_Table_UserList_TableRows = new By.ByXPath(".//tbody/tr[@class='filterRow' and (not(@style) or @style='display: table-row;')]");
	public List<String> getListOfUsers() throws Exception{
		List<String> listofusers = new ArrayList<String>();
		//List<WebElement> rows_user = getTableUserList().findElements(By.xpath(".//tbody/tr[@class='filterRow' and (not(@style) or @style='display: table-row;')]"));
		List<WebElement> rows_user = ReturnSubWebElements("User Rows", by_Table_UserList, by_Table_UserList_TableRows, GlobalShortWait,GlobalShortRetry); 
				
		if (rows_user.size()==0) return null;
			
		for(WebElement e: rows_user){
			String s = "";
			s = s + e.findElement(By.xpath("./td[1]")).getText();
			s = s + "|" + e.findElement(By.xpath("./td[2]")).getText();
			s = s + "|" + e.findElement(By.xpath("./td[3]")).getText();
			listofusers.add(s);
		}
		return listofusers;
	}
	
	public void passwordField(){
		driver.findElement(By.xpath("*//tr/td[text()='Password:']"));
	}
	
	public void enterPasssword(String password){
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
	}
	
	public void enterConfirmPassword(String confirmpassword){
		driver.findElement(By.xpath("//input[@name='confirmPassword']")).sendKeys(confirmpassword);
	}
	

	public void userSaved() throws Exception{
		
		Assert.assertTrue(getError().getText().equals("Data saved"));
	}
	
	
}
