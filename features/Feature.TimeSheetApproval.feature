@FeatureTimesheetApprove
Feature: Time Sheet Approval

@TimeSheetApproval
Scenario: Time Sheet Approver
    Given Open browser and start application
    When I enter valid "admin" and "admin"
    Then I should be able see "Admin, eHour" on Welcome Page
    When I select menu item "Users"
    Then Page Header must display "User management"

  When I add a new user to fill timesheet with the following details if non existent
      |Username|FirstName|LastName|Email|Password|ConfirmPassword|FunctionalGroup|UserRole|Active|AssignToProjectAfterSave|Country|Location|EmployeeType|TimeSheetApprover|
      | JamesUser1   | Jamescu1     | Cameronu1  | Jamecamu1@nisum.com | yahoo$123 | yahoo$123       | Offshore   | User|true   | false                   | Pakistan | Karachi  | Nisum        |Admin, eHour		 |

  When I add a new user to fill timesheet with the following details if non existent
      |Username|FirstName|LastName|Email|Password|ConfirmPassword|FunctionalGroup|UserRole|Active|AssignToProjectAfterSave|Country|Location|EmployeeType|TimeSheetApprover|
      | JamesUser2   | Jamescu2     | Cameronu2  | Jamecamu2@nisum.com | yahoo$123 | yahoo$123       | Offshore   | User|true   | false                   | Pakistan | Karachi  | Nisum        |Cameronu1, Jamescu1		 |

#    When I select menu item "Assignments"
#    When I assign projects to "user2" as follows
#      |Account             | Project                   | AssignmentType | StartDate | EndDate | Role | HourlyRate | Active |
#      | gap - GAP 				 | PROJ4626 - CCLO - Brampton | Date range     | true      | true    | QA   | 60.00       | true   |
#    Then I click save button on user page
    When I logout from eHour
    Then I am successfully logged out

    Given Open browser and start application
  When I enter valid "JamesUser2" and "yahoo$123"
  Then I should be able see "Cameronu2, Jamescu2" on Welcome Page
    And I am navigated to Timesheet page
#    And I see the following accounts-projects in the timesheet
#      | AccountName    | ProjectName |
#      | GAP						 | CCLO - Brampton |
#    When I enter Timesheet Data under Account "GAP" as follows
#      | Project     | Sun | Mon | Tue | Wed | Thu | Fri | Sat |
#      | CCLO - Brampton | 8   | 8   | 8   | 8   | 8   | 8   | 8   |
  When I Fill the hours for the project "PTO"
  And I enter the comment "I am working on Automation"
  When I store the Timesheet Data
  Then I check if the successful prompts is displayed or not
    When I logout from eHour
    Then I am successfully logged out

    Given Open browser and start application
  When I enter valid "JamesUser1" and "yahoo$123"
  Then I should be able see "Cameronu1, Jamescu1" on Welcome Page
    When I click on Approve Timesheet
    Then I should see "Cameronu2, Jamescu2" timesheet details for the respective week
  Then I should see status on approvertimesheet as "POSTED"
    When I click on reject Anchor
    When I logout from eHour
    Then I am successfully logged out

    Given Open browser and start application
  When I enter valid "JamesUser2" and "yahoo$123"
  Then I should be able see "Cameronu2, Jamescu2" on Welcome Page
    Then I should see the status as "REJECTED"
    When I logout from eHour
    Then I am successfully logged out


    Given Open browser and start application
  When I enter valid "JamesUser1" and "yahoo$123"
  Then I should be able see "Cameronu1, Jamescu1" on Welcome Page
    When I click on Approve Timesheet
    Then I should see status on approvertimesheet as "REJECTED"
    When I click on approve Anchor
    When I logout from eHour
    Then I am successfully logged out


#    Given Open browser and start application
#  When I enter valid "JamesUser1" and "yahoo$123"
#  Then I should be able see "Cameronu1, Jamescu1" on Welcome Page
#    When I click on Approve Timesheet
#    Then I should see status on approvertimesheet as "APPROVED"
#    When I logout from eHour
#    Then I am successfully logged out

    Given Open browser and start application
  When I enter valid "JamesUser2" and "yahoo$123"
  Then I should be able see "Cameronu2, Jamescu2" on Welcome Page
  Then I should see the status as "APPROVED"
    When I logout from eHour
    Then I am successfully logged out

#    Given Open browser and start application
#    When I enter valid "admin" and "admin"
#    When I select menu item "Users"
#    Then Page Header must display "User management"
#    When I Select the users to delete
#			|UserName|
#			|user1|
#		Then I cannot search the users
#			|UserName|
#			|user1|
#    When I logout from eHour
#    Then I am successfully logged out
