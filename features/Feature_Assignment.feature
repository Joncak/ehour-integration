@FeatureAssignment
Feature: Assigning Users to Projects

	@ProjectAssignment
	Scenario: Create Users and assign them to project
		Given Open browser and start application
		When I enter valid "admin" and "admin"
		Then I should be able see "Admin, eHour" on Welcome Page
		When I select menu item "Users"
		Then Page Header must display "User management"
		When I add a new user with the following details if non existent
			| Username   | FirstName | LastName | Email             | Password  | ConfirmPassword | Department | UserRole | Active | AssignToProjectAfterSave | Country  | Location | EmployeeType |TimeSheetApprover|
			| JamesCam   | Jamesc     | Cameron  | Jamecam@nisum.com | yahoo$123 | yahoo$123       | Offshore   | User     | true   | false                   | Pakistan | Karachi  | Nisum        |Admin, eHour		 |

		Then I select menu item "Assignments"
		And I assign projects to users as follows
			|FirstName|LastName|Account  |Project                   |AssignmentType|StartDate|EndDate|Role     |HourlyRate|Active|
			|Jamesc   |Cameron |gap - GAP|PROJ4626 - CCLO - Brampton|Date range    |true     |true   |Developer|60.00      |true |

		When I logout from eHour
		Then I am successfully logged out
		When I enter valid "JamesCam" and "yahoo$123"
		Then I should be able see "Cameron, Jamesc" on Welcome Page
		And I am navigated to Timesheet page

		Then I see the following accounts-projects in the timesheet
			| AccountName    | ProjectName 								|
			| GAP            | CCLO - Brampton 						|

		When I logout from eHour
		Then I am successfully logged out


		When I enter valid "admin" and "admin"
		Then I should be able see "Admin, eHour" on Welcome Page

		When I select menu item "Assignments"
		When I edit assignments as follows
			|FirstName	|LastName	|Account	|Project									 |AssignmentType |StartDate|EndDate|Role	|HourlyRate|Active|
			|Jamesc			|Cameron	|gap - GAP|PROJ4626 - CCLO - Brampton|Date range		 |true		 |true	 |Tester|10.00		 |true	|
		When I unassign projects to users as follows
			|FirstName	|LastName	|Account	|Project									 |AssignmentType |StartDate|EndDate|Role		 |HourlyRate|Active|
			|Jamesc			|Cameron 	|gap - GAP|PROJ4626 - CCLO - Brampton|Date range		 |true		 |true	 |Developer|0.00		  |true	 |

		When I select menu item "Users"
		Then Page Header must display "User management"
		When I Select the user with username - "Cameron"
		And I press the delete button
		Then I cannot search the user with the username - "Cameron"

		When I logout from eHour
		Then I am successfully logged out

	@ProjectAssignmentwithdates

	Scenario: Assigned project should not be visible in the user's time sheet after it's end date
		Given Open browser and start application
		When I enter valid "admin" and "admin"
		Then I should be able see "Admin, eHour" on Welcome Page
		When I select menu item "Users"
		Then Page Header must display "User management"
		When I add a new user with the following details if non existent
			| Username   | FirstName | LastName | Email             | Password  | ConfirmPassword | Department | UserRole | Active | AssignToProjectAfterSave | Country  | Location | EmployeeType |TimeSheetApprover|
			| JamesCam   | Jamesc    | Cameron | Jamecam@nisum.com | yahoo$123 | yahoo$123       | Offshore   | User     | true   | false                   | Pakistan | Karachi  | Nisum        |Admin, eHour		 |

		Then I select menu item "Assignments"
		And I assign projects to users as follows date based
			|FirstName|LastName|Account  |AssignmentType|Role     |HourlyRate|Active|
			|Jamesc   |Cameron |gap - GAP|Date range     |Developer|60.00      |true |

		When I logout from eHour
		Then I am successfully logged out
		When I enter valid "JamesCam" and "yahoo$123"
		Then I should be able see "Cameron, Jamesc" on Welcome Page
		And I am navigated to Timesheet page

		When I logout from eHour
		Then I am successfully logged out

	@ProjectAssignmentStartdatecannottbeafterenddate
	Scenario: Verifying that end date should be always >start date

		Given Open browser and start application
		When I enter valid "admin" and "admin"
		Then I should be able see "Admin, eHour" on Welcome Page
		When I select menu item "Users"
		Then Page Header must display "User management"
		When I add a new user with the following details if non existent
			| Username| FirstName| LastName| Email             | Password  | ConfirmPassword | Department | UserRole | Active | AssignToProjectAfterSave | Country  | Location | EmployeeType |TimeSheetApprover|
			| JamesCameron| James| Cameron| JamesCameron@nisum.com | yahood$123 | yahood$123       | Offshore   | User     | true   | false                   | Pakistan | Karachi  | Nisum        |Admin, eHour       |

		When I select menu item "Assignments"

		When I assign projects to users as follows with dates
			|FirstName|LastName|Account| AssignmentType | StartDate |EnterStartDate| EndDate |EnterEndDate| Role | HourlyRate | Active |
			|James|Cameron| gap - GAP|  Date range     | false      |8/30/2017| false    |8/1/2017| QA   | 60.00       | true   |


		Then I should see error message for the dates
		When I logout from eHour
		Then I am successfully logged out
