@FeatureLockedPeriodsManagement
Feature: Locked Period Management 

@ScenarioLockedPeriodsManagement
Scenario: Verify that locked period causes the timesheet to be locked for the given period.
	
	Given Open browser and start application
	When I enter valid "admin" and "admin" 
	Then I should be able see "Admin, eHour" on Welcome Page
	When I select menu item "Users" 
	Then Page Header must display "User management"
	When I add a new user with the following details if non existent
  | Username   | FirstName | LastName | Email             | Password  | ConfirmPassword | Department | UserRole | Active | AssignToProjectAfterSave | Country  | Location | EmployeeType |TimeSheetApprover|
 	| JamesCam   | Jamesc     | Cameron  | Jamecam@nisum.com | yahoo$123 | yahoo$123       | Offshore   | User     | true   | false                   | Pakistan | Karachi  | Nisum        |Admin, eHour		 |
	When I select menu item "Locked periods" 
	Then Page Header must display "Locked periods"
	When I search and delete the locked periods for current week
	And I create new locked period for current week
	When I logout from eHour
	
	When I enter valid "JamesCam" and "yahoo$123"
	Then I should be able see "Cameron, Jamesc" on Welcome Page 
	And I am navigated to Timesheet page 
	And I see that the fields are locked for the current week
	When I logout from eHour
	
	And I enter valid "admin" and "admin"
	And I select menu item "Locked periods" 
	Then Page Header must display "Locked periods"
	When I search and delete the locked periods for current week
	And I logout from eHour
	
	When I enter valid "JamesCam" and "yahoo$123"
	Then I should be able see "Cameron, Jamesc" on Welcome Page  
	And I am navigated to Timesheet page 
	And I see that the fields are unlocked for the current week
	Then I logout from eHour
	Then I am successfully logged out
	
	When I enter valid "admin" and "admin"
  Then I should be able see "Admin, eHour" on Welcome Page
  When I select menu item "Users"
	Then Page Header must display "User management"
	When I Select the user with username - "Cameron"
  And I press the delete button
  Then I cannot search the user with the username - "Cameron"	
	When I logout from eHour
	Then I am successfully logged out