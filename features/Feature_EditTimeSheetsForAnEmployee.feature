@EditTimeSheetOfEmployee
Feature: Edit time sheets for other employees
	@EditTimeSheets
  Scenario: Creates an user and edit timesheets with admin rights
    Given Open browser and start application
    When I enter valid "admin" and "admin"
    Then I should be able see "Admin, eHour" on Welcome Page
    When I select menu item "Users"
    Then Page Header must display "User management"
    When I add a new user to fill timesheet with the following details if non existent
        |Username|FirstName|LastName|Email|Password|ConfirmPassword|FunctionalGroup|UserRole|Active|AssignToProjectAfterSave|Country|Location|EmployeeType|TimeSheetApprover|
        |QAUser|User|DontDelete|QAUser@nisum.com|Nisum$123|Nisum$123|Offshore|User|true|false|Pakistan|Karachi|Nisum|Admin, eHour|
    Then I select menu item "Edit timesheet"
    Then I select a user "DontDelete" to edit the timesheet
    Then I click on Temporarily sign in and check if i logged in temporarily to that user i should see "DontDelete, User"
    When I Fill the hours for the project "PTO"
    And I enter the comment "I am working on Automation"
    When I store the Timesheet Data
    Then I check if the successful prompts is displayed or not
    Then I switch back to the admin account
    When I logout from eHour
    Then I am successfully logged out
    When I enter valid "QAUser" and "Nisum$123"
    Then I should be able see "DontDelete, User" on Welcome Page
    And I am navigated to Timesheet page
    Then I check if the total hours filled are "56.00" or not
    When I logout from eHour
    Then I am successfully logged out
    
