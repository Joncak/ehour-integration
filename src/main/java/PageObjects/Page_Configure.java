package PageObjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static PageObjects.Base.*;


public class Page_Configure {

    String GetFirstDayofWeek;
    static String GetAdminFirstDayofWeek;

    static By by_UserWeek = new By.ByXPath("//td[@class='day'][1]/div/div[@class='name']");//sun


    public String storeUserWeek() throws Exception {

        GetFirstDayofWeek = ReturnWebElement("User:First day of week", by_UserWeek, GlobalShortWait, GlobalShortRetry, null).getText();
        return GetFirstDayofWeek;

    }

    static By by_AdminWeek = new By.ByName("firstWeekStart");

    public WebElement getAdminWeek() throws Exception {
        Log.info("$$$$$$$$$$$$$$$$$$$$$$ : " + by_AdminWeek);
        return ReturnWebElement("Admin:First day of week", by_AdminWeek, GlobalShortWait, GlobalShortRetry, null);

    }

    public void compareAdminandUserWeek() throws Exception {

        Thread.sleep(5000);

        GetAdminFirstDayofWeek = getAdminWeek().getText();
        Log.info("$$$$$$$$$$$$$$$$$$$$$$ : " + GetAdminFirstDayofWeek);
        Assert.assertTrue("GetAdminFirstDayofWeek contains GetFirstDayofWeek", GetAdminFirstDayofWeek.contains(GetFirstDayofWeek));
    }

    public void selectDay(String firstday) throws Exception {
        Select dayDropDown = new Select(getAdminWeek());
        dayDropDown.selectByVisibleText(firstday);
    }

}
