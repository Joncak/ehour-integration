@FeatureManageProjects
Feature: Project Management Feature

@ProjectManagement
Scenario: Project Management
	Given Open browser and start application 
	When I enter valid "admin" and "admin"
	And I select menu item "Projects" 
	Then Page Header must display "Project management"
	 
	When I add the following non-existent projects
	|Name|ProjectCode|Client|ProjectManager|Description|ContactPerson|DefaultProject|Billable|Active|
	|Fordproject|FPC|NOB - Nisum Non-Billable|No projectmanager|Ford Mustang GT project|Henry Ford|false|false|true|
	
	Then I can search the following projects
	|Name|ProjectCode|
	|Fordproject|FPC|
	
	
	When I modify the following projects
	|Name|ProjectCode|Client|ProjectManager|Description|ContactPerson|DefaultProject|Billable|Active|
	|Fordproject|FPC|NOB - Nisum Non-Billable|No projectmanager|Ford Explorer|Ford Jr.|true|false|true|
	
	When I delete the following projects
	|Name|ProjectCode|
	|Fordproject|FPC|
	
	When I logout from eHour 
	Then I am successfully logged out
	