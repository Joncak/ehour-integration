package stepDefinition;

import PageObjects.Page_Reports;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class Steps_Reports {
    Page_Reports pr;

    @When("^I click menu item reporting$")
    public void iClickMenuItem() throws Throwable {
        pr = new Page_Reports();
        pr.getMenuReporting().click();

    }

    @Then("^Page shows tab as \"([^\"]*)\"$")
    public void pageShowsTabAs(String Tab1) throws Throwable {
        Assert.assertTrue(pr.getTabGlobalReport().equals(Tab1));
    }


    @Then("^I enter start date \"([^\"]*)\" and end date \"([^\"]*)\"$")
    public void iSelectEnterDateAndEndDate(String startdate, String enddate) throws Throwable {

        pr.getStartDateEndDate(startdate,enddate);

    }

    
    @When("^I select Clients on Global Report page$")
    public void i_select_clients(DataTable dt) throws Exception{
    	for(int i=1;i<dt.raw().size();i++){
    		pr.selectClients(dt.raw().get(i));
    	}
    }
    
    @When("^I select Users on Global Report page$")
    public void iSelectUsersOnGlobalReportPage(DataTable users) throws Throwable {
    	for(int i=1;i<users.raw().size();i++){
    		pr.selectSpecificUsers(users.raw().get(i));
    	}

    }

    @Then("^I should see appropriate change in Project filter$")
    public void iShouldSeeAppropriateChangeInProjectFilter() throws Throwable {
          pr.storeAllProjects();
    }

    @When("^I click create report button$")
    public void iClickCreateReportButton() throws Throwable {
        pr.clickReport();

    }

    @Then("^I should see tab as \"([^\"]*)\"$")
    public void iShouldSeeTabAs(String clientreport) throws Throwable {
        Assert.assertTrue(pr.getTabClientReport().equals(clientreport));

    }

    @And("^I should see \"([^\"]*)\"$")
    public void iShouldSee(String reportperiod) throws Throwable {
        Assert.assertTrue(pr.getReportPeriod().equals(reportperiod));
    }


    @Then("^I should see same selected projects on Client report$")
    public void iShouldSeeSameSelectedProjectsOnReportPage() throws Throwable {

        pr.compareProjectsGlobalReportAndClientReport();

    }

    @Then("^I should see \"([^\"]*)\" Client on Client report$")
    public void iShouldSeeClientOnReportPage(String clientname) throws Throwable {

        Assert.assertTrue(pr.getVisibleClient().equals(clientname));

    }
    
    @Then("^I see the following records on generated Client report$")
    public void i_store_values_of_the_report(DataTable dt){
    		System.out.println("########################################################################");
    		pr.getValuesofthereport(dt);
    }
    
    @And("^I validate total hours \"([^\"]*)\" and turnover \"([^\"]*)\" for the selected project,users combination on the report$")
    public void i_validate_total_hours_and_turnover(String hours, String turnover){
    	pr.validatehoursandturnover(hours, turnover);
    }
}
