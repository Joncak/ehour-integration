@FeatureUserManagement
Feature: User Management Feature 


@CreateDeleteUser
Scenario: Create and Delete User
	
		Given Open browser and start application 
		When I enter valid "admin" and "admin"
		Then I should be able see "Admin, eHour" on Welcome Page
		When I select menu item "Users" 
		Then Page Header must display "User management" 
		When I add a new user with the following details if non existent
	    | Username   | FirstName | LastName | Email             | Password  | ConfirmPassword | Department | UserRole | Active | AssignToProjectAfterSave | Country  | Location | EmployeeType |TimeSheetApprover|
	 	| JamesCam   | Jamesc     | Cameron  | Jamecam@nisum.com | yahoo$123 | yahoo$123       | Offshore   | User     | true   | false                   | Pakistan | Karachi  | Nisum        |Admin, eHour		 |
		When I logout from eHour 
		Then I am successfully logged out
		
		When I enter valid "JamesCam" and "yahoo$123"
		Then I should be able see "Cameron, Jamesc" on Welcome Page 
		When I logout from eHour 
		Then I am successfully logged out
		 
		When I enter valid "admin" and "admin" 
		Then I should be able see "Admin, eHour" on Welcome Page 
		When I select menu item "Users"
	  Then Page Header must display "User management"
      When I edit a user with the following details
      |Username|FirstName|LastName|Email|Password|ConfirmPassword|Department|UserRoles|Active|Country|Location|EmployeeType|TimesheetApprover|
      |JamesCam||Cameron||Nisum$123|Nisum$123|||true|||Nisum||
#	  When I Select the user with username - "Cameron"
#      When I press the delete button
#      Then I cannot search the user with the username - "Cameron"
		When I logout from eHour
		Then I am successfully logged out

						
						
		