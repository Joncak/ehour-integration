package stepDefinition;

import cucumber.api.PendingException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import PageObjects.Base;
import PageObjects.Log;
import PageObjects.Page_Client;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class Steps_ClientManagement  {

	Page_Client pc = new Page_Client("Anything");
	
	
@When("^I create a client with the following details$")
public void i_create_a_client_with_the_following_details(DataTable table) throws Throwable {
    pc.CreateClient(table);
    Log.info("Created a new client with the required details");
}

@Then("^I can search the client by Client Name \"([^\"]*)\"$")
public void i_can_search_the_client_by_Client_Name(String ClientName) throws Throwable {
	pc.VerifyIfClientRecordCanBeFoundInSearchResults(ClientName);
	Log.info("Searched the newly created Client:"+ClientName);
}

@When("^I select the client with Client Name \"([^\"]*)\"$")
public void i_select_the_client_with_Client_Name(String ClientName) throws Throwable {
	pc.SelectClientRowFromSearchResults(ClientName);
}

@Then("^The selected client has the following details$")
public void the_selected_client_has_the_following_details(DataTable table) throws Throwable {
	pc.VerifySelectedClient(table);
	Log.info("Validated the client details");
}

@When("^I delete the client with Client Name \"([^\"]*)\"$")
public void i_delete_the_client_with_Client_Name(String ClientName) throws Throwable {
		BaseSteps.AssertTrue("Desired Client Row in the List of All Clients", true, pc.getDesiredClient(ClientName)!=null, pc.getDesiredClient(ClientName)!=null);
		pc.getDesiredClient(ClientName).click();
		//Thread.sleep(2000);
		pc.DeleteClient(ClientName);
		Log.info("Deleted the client :"+ClientName);
}




@Then("^I cannot search the client by Client Name \"([^\"]*)\"$")
public void i_cannot_search_the_client_by_Client_Name(String ClientName) throws Throwable {
	BaseSteps.AssertTrue("Desired Client Row", true, pc.getDesiredClient(ClientName) == null, pc.getDesiredClient(ClientName) == null);
	//Assert.assertTrue("Client could be found", pc.getDisplayedFilteredClientCount().equals(0));
}

@When("^I delete a client named \"([^\"]*)\"$")
public void i_delete_a_client_named(String ClientName) throws Throwable {
	if (pc.getDesiredClient(ClientName)!=null){
		pc.getDesiredClient(ClientName).click();
		pc.DeleteClient(ClientName);
		}
	}

	@Then("^I check if the client named \"([^\"]*)\" is displayed while creating a project$")
	public void iCheckIfTheClientNamedIsDisplayedWhileCreatingAProject(String ClientName) throws Throwable {
		pc.CheckClientname(ClientName);
	}

	@Then("^I make \"([^\"]*)\" client inactive$")
    public void iMakeClientInactive(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I check if the \"([^\"]*)\" is displayed in the list$")
    public void iCheckIfTheIsDisplayedInTheList(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I make the \"([^\"]*)\" active again$")
    public void iMakeTheActiveAgain(String client) throws Throwable {
		pc.MakeClientActive(client);
    }

	@Then("^I check if \"([^\"]*)\" is present in the exported excel data$")
	public void iCheckIfOsPresentInTheExportedExcelData(String Client) throws Throwable {
		pc.ExportExcel(Client);
	}

	@Then("^I check if the \"([^\"]*)\" is displayed or not$")
	public void iCheckIfTheIsDisplayedOrNot(String client) throws Throwable {
		pc.SearchInactiveCleint(client);
	}

	@Then("^I make the client inactive$")
	public void iMakeTheClientInactive() throws Throwable {
		pc.MakeInactive();
	}
}
