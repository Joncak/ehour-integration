package stepDefinition;

import PageObjects.Log;
import PageObjects.Page_Timesheet;
import PageObjects.Page_UserManagement;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static PageObjects.Base.driver;
import static stepDefinition.BaseSteps.AssertTrue;

public class Steps_Timesheet {

	Page_Timesheet pt = new Page_Timesheet("anything");
	Page_UserManagement pum;

	
	
	
@Then("^I am navigated to Timesheet page$")
public void i_am_navigated_to_Timesheet_page() throws Throwable {
	Assert.assertTrue(pt.IsTimesheetPageDisplayed());
	Log.info("Navigated to Timesheet Page");
}


@Then("^I see the dates of the current week$")
public void i_see_the_dates_of_the_current_week() throws Throwable {
    Assert.assertTrue(pt.MatchCurrentWeekDaysAndDates());
}

//@When("^I enter Timesheet Data under Account \"([^\"]*)\" as follows$")
//public void i_enter_Timesheet_Data_under_Account_as_follows(String AccountName, DataTable table) throws Throwable {
//	pt.FillTimeSheetData(AccountName, table);
//	Log.info("Entered Hours in the respective sections");
//}

//below i added a method

    @Then("^I should see user name \"([^\"]*)\" as follows$")
    public void i_should_see_user_name_as_follows(String USERNAME) throws Throwable {
        pt.verifyUsernameToApproveTimesheet(USERNAME);
        Log.info("Verified "+USERNAME+" to Approve Timesheet");
    }
    
    @Then("^I should see second user timesheet details for the respective week$")
    public void i_should_see_second_user_name_as_follows() throws Throwable {
    	String USERNAME=pum.unique_lastname_u2+", "+pum.unique_firstname_u2;
        pt.verifyUsernameToApproveTimesheet(USERNAME);
        System.out.println("Verified "+USERNAME+" to Approve Timesheet");
    }
    

    //below iadded
    @Then("^I should see the status as \"([^\"]*)\"$")
    public void i_should_see_the_status_as(String STATUSCODE) throws Throwable {
    Assert.assertTrue(pt.verifyStatusCodeAfterApproval().getText().equals(STATUSCODE),"The status is not in approved" +
            " state");
        System.out.println("Status of the user timesheet is : "+STATUSCODE);
    }

    //below i added
    @Then("^I should see status on approvertimesheet as \"([^\"]*)\"$")
    public void i_should_see_status_on_approvertimesheet_as(String LABEL) throws Throwable {
    Assert.assertTrue(pt.verifyLabelOnApproverTimeSheet().getText().equals(LABEL),"Failed to verify the staus"+LABEL);
        System.out.println("Status of the user timesheet is : "+LABEL);
    }

    /*
    //below i added a method
    @Then("^I remove the filled timesheet again to delete the user and click on save$")
    public void i_remove_the_filled_timesheet_again_to_delete_the_user_and_click_on_save(DataTable empty) throws Throwable {
        pt.ClearTimeSheetDataTable(empty);
        pt.EmptyingTimesheet();
    }*/

    @When("^I enter the comments - \"([^\"]*)\"$")
    public void i_enter_the_comments(String arg1) throws Throwable {

    }

@When("^I store the Timesheet Data$")
public void i_store_the_Timesheet_Data() throws Throwable {
  pt.StoreTime();
  Log.info("Stores the timesheet successfully!");
}

@Then("^the Timesheet Data under Account \"([^\"]*)\" is stored as follows$")
public void the_Timesheet_Data_under_Account_is_stored_as_follows(String AccountName, DataTable table) throws Throwable {	
	Assert.assertTrue(pt.CheckEnteredTimesheet(AccountName, table), "Stored timesheet is different from expected timesheet");
}


@Then("^I see the following accounts-projects in the timesheet$")
public void i_see_the_following_accounts_projects_in_the_timesheet(DataTable dt) throws Throwable {

	for(int i=1;i < dt.raw().size();i++){
		AssertTrue("Account Specific Project Row", "Not Null",pt.getProjectSpecificRowUnderAccount(dt.raw().get(i).get(0), dt.raw().get(i).get(1))!=null?"Not Null":"Null" , pt.getProjectSpecificRowUnderAccount(dt.raw().get(i).get(0), dt.raw().get(i).get(1))!=null);
		System.out.println(pt.getProjectSpecificRowUnderAccount(dt.raw().get(i).get(0), dt.raw().get(i).get(1)));
	}
		Log.info("Verified the account-projects in the user's timesheet");

	}


	@When("^I enter the comment \"([^\"]*)\"$")
		public void i_enter_the_comment(String Comment) throws Throwable {
		pt.fillComments(Comment);
	}

	@Then("^the retrieved comment is \"([^\"]*)\"$")
	public void the_retrieved_comment_is(String Comment) throws Throwable {
	   pt.VerifyComments(Comment);
	}

	@When("^I set Week Location to \"([^\"]*)\"$")
	public void i_set_Week_Location_to(String Location) throws Throwable {
		pt.SetWorkLocation(Location);
	}

	@Then("^I see the week location as \"([^\"]*)\"$")
	public void i_see_the_week_location_as(String Location) throws Throwable {
		AssertTrue("Work Location", Location, pt.getWorkLocation(), pt.getWorkLocation().equals(Location));
	}

    @When("^I Fill the hours for the project \"([^\"]*)\"$")
    public void iFillTheHoursForTheProject(String ProjectName) throws Throwable {
        pt.FillTimeSheetData(ProjectName);
    }

    @Then("^I check if the successful prompts is displayed or not$")
    public void iCheckIfTheSuccessfulPromptsIsDisplayedOrNot() throws Throwable {
        pt.VerifyTimesheetPrompt();
    }

    @Then("^I check if the total hours filled are \"([^\"]*)\" or not$")
    public void iCheckIfTheTotalHoursFilledAreOrNot(String TotalHours) throws Throwable {
        pt.VerifyTotalHours(TotalHours);
    }

    @Then("^I should see \"([^\"]*)\" timesheet details for the respective week$")
    public void iShouldSeeTimesheetDetailsForTheRespectiveWeek(String USERNAME) throws Throwable {
        pt.verifyUsernameToApproveTimesheet(USERNAME);
        System.out.println("Verified "+USERNAME+" to Approve Timesheet");
    }

    private static final DateFormat dateFormat = new SimpleDateFormat("MMMMMMMM");
    @Then("^I should not see the following accounts-projects in the timesheet$")
    public void i_shouldnot_see_the_following_accounts_projects_in_the_timesheet(DataTable dt) throws Throwable {

        Date currentDate = new Date();
        Thread.sleep(3000);
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);

        // manipulate date
        c.add(Calendar.YEAR, 0);
        c.add(Calendar.MONTH, 2);
        c.add(Calendar.DATE, 0); //same with c.add(Calendar.DAY_OF_MONTH, 1);
        c.add(Calendar.HOUR, 0);
        c.add(Calendar.MINUTE, 0);
        c.add(Calendar.SECOND, 0);

        Date currentDatePlusTwo = c.getTime();
        Log.info("qqqqqqqqqq"+currentDatePlusTwo);
        String MonthName = dateFormat.format(currentDatePlusTwo);
        Thread.sleep(5000);
        Log.info("venu" +MonthName);


        String monthclick = "//*[@class='CalendarHeader']/div[3]/a";
        driver.findElement(new By.ByXPath(monthclick)).click();
        Thread.sleep(3000);
        driver.findElement(new By.ByXPath(monthclick)).click();
        Thread.sleep(3000);
        String month = "//*[@class='CalendarHeader']/div[2]";
        String nextMonth = driver.findElement(new By.ByXPath(month)).getText();

        Assert.assertEquals("December 2017", nextMonth);
        AssertTrue("December","December", nextMonth,MonthName.contains("December"));

        WebElement project = driver.findElement(new By.ByXPath("//*[@id='timesheetForm']/div[2]/div/div[3]/table/tbody/tr[2]/td[2]/span[1]"));
        Thread.sleep(5000);
        Assert.assertEquals(true, project.isDisplayed());

    }
}
