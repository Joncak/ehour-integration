@FeatureManageClients
Feature: Client Management 

@ClientManagement
Scenario: Client - Add, Search, Delete

	Given Open browser and start application
	When I enter valid "admin" and "admin"
	Then I should be able see "Admin, eHour" on Welcome Page
	When I select menu item "Clients"
	Then Page Header must display "Client management"
	When I delete a client named "TestClient"
	When I create a client with the following details
	|ClientName|ClientCode|ClientDescription|IsActive|
	|FordMustang|FMG|Mustang GT production|true|
	Then I can search the client by Client Name "FordMustang"
	When I select the client with Client Name "FordMustang"
	Then The selected client has the following details
	|ClientName|ClientCode|ClientDescription|IsActive|
	|FordMustang|FMG|Mustang GT production|true|
	When I select menu item "Projects"
	Then Page Header must display "Project management"
	Then I check if the client named "FordMustang" is displayed while creating a project
	When I select menu item "Clients"
	Then Page Header must display "Client management"
	When I delete the client with Client Name "FordMustang"
	Then I cannot search the client by Client Name "FordMustang"
	When I logout from eHour
	Then I am successfully logged out

	@ClientbuttonsVerification
	Scenario: Check for the only active clients and export to excel button scenario
		Given Open browser and start application
		When I enter valid "admin" and "admin"
		Then I should be able see "Admin, eHour" on Welcome Page
		When I select menu item "Clients"
		Then Page Header must display "Client management"
        When I delete a client named "FordMustang"
		When I create a client with the following details
			|ClientName|ClientCode|ClientDescription|IsActive|
			|FordMustang|FMG|Mustang GT production|true|
		Then I check if "FordMustang" is present in the exported excel data
        When I select the client with Client Name "FordMustang"
        Then I make the client inactive
        Then I check if the "FordMustang" is displayed or not
        Then I make the "FordMustang" active again
		When I delete the client with Client Name "FordMustang"
		When I logout from eHour
		Then I am successfully logged out