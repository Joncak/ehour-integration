package stepDefinition;

import PageObjects.Log;
import PageObjects.Page_Configure;
import PageObjects.Page_Header;
import cucumber.api.java.en.Then;
import org.junit.Assert;


public class Steps_Configuration {


    Page_Header h = new Page_Header("anything");
    Page_Configure config;


    @Then("^I should see value First_day_of_week on timesheet page$")
    public void userShouldSeeValue() throws Throwable {
        config = new Page_Configure();
        Log.info("First Day of Week on Timesheet Page is : "+config.storeUserWeek());
    }


    @Then("^I should see value of First_day_of_week on timeSheet page to be same as on configure page$")
    public void iShouldSeeValueFirstDayOfWeek() throws Throwable {

        Thread.sleep(5000);
        config.compareAdminandUserWeek();
        Log.info("Compared the Value of First Day of week on Timesheet Page with Congiure Page");
    }

    @Then("^I update First_day_of_week on Configure Page as \"([^\"]*)\"$")
    public void iUpdateFirstDayOfWeek(String firstday) throws Throwable {
        config.selectDay(firstday);
        Log.info("Updated First Day of Week to "+firstday+" on the Configure Page");
    }

    @Then("^Page should display \"([^\"]*)\"$")
    public void pageShouldDisplay(String PageTitle) throws Throwable {
        Assert.assertTrue(h.getPageTitle().equals(PageTitle));
    }

}

