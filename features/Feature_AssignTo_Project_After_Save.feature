@FeatureAssignProjectAfterSave
Feature: Assign To Project After Save

  @AssignProjectAfterSave
  Scenario: Assign To Project After Save
    Given Open browser and start application
    When I enter valid "admin" and "admin"
    Then I should be able see "Admin, eHour" on Welcome Page
    When I select menu item "Users"
    Then Page Header must display "User management"
    When I add a new user with the following details if non existent
  	| Username   | FirstName | LastName | Email             | Password  | ConfirmPassword | Department | UserRole | Active | AssignToProjectAfterSave | Country  | Location | EmployeeType |TimeSheetApprover|
 		| JamesCam   | Jamesc    | Cameron  | Jamecam@nisum.com | yahoo$123 | yahoo$123       | Offshore   | User     | true   | true                     | Pakistan | Karachi  | Nisum        |Admin, eHour		 |
		
		Then Page Header must display "Assignment management"
    When I assign projects to users as follows 
		|FirstName|LastName|Account  |Project                   |AssignmentType|StartDate|EndDate|Role     |HourlyRate |Active|
		|Jamesc   |Cameron |gap - GAP|PROJ4626 - CCLO - Brampton|Date range    |true     |true   |Developer|60.00      |true  |
    Then I click save button on user page
    When I logout from eHour
    Then I am successfully logged out
    
    Given Open browser and start application
    When I enter valid "JamesCam" and "yahoo$123"
		Then I should be able see "Cameron, Jamesc" on Welcome Page
    And I am navigated to Timesheet page
    And I see the following accounts-projects in the timesheet
      | AccountName    | ProjectName     | 
      | GAP						 | CCLO - Brampton |
    When I logout from eHour
    Then I am successfully logged out

    Given Open browser and start application
    When I enter valid "admin" and "admin"
    When I select menu item "Users"
	  Then Page Header must display "User management"
    When I Select the user with username - "Cameron"
    When I press the delete button
    Then I cannot search the user with the username - "Cameron"	
		When I logout from eHour
		Then I am successfully logged out

