package PageObjects;

import cucumber.api.DataTable;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class Page_Client extends Base {
	
	public Page_Client(String BrowserType) {
		super(BrowserType);
	}
	By by_TextBox_CustomerName = new By.ByXPath("//input[@name='customer.name']");
	By by_TextBox__CustomerCode = new By.ByXPath("//input[@name='customer.code']");
	By by_TextBox_CustomerDescription = new By.ByXPath("//textarea[@name='customer.description']");
	By by_CheckBox_CustomerActive = new By.ByXPath("//input[@name='customer.active']");
	By by_TextBox_Filter = new By.ById("listFilter");
	By by_Table_ClientList = new ByXPath("//div[@id='listContents']/table[@class='entrySelectorTable']");
	By by_Table_ClientList_DisplayedRows = new By.ByXPath(".//tbody/tr[@class='filterRow' and (not(@style) or @style='display: table-row;')]");
	String xpath_ClientList_SelectRowByClientName=".//tbody/tr[@class='filterRow' and (not(@style) or @style='display: table-row;') and (./td[text()='REPLACE'][1])]";
	By by_Table_ClientList_SelectRowByClientName;

	By by_Error = new By.ByXPath("//span[@class='formValidationError']");
	
	
	
	String xpath_Row_DesiredClient = ("./tbody//tr[@class='filterRow' or @class='selectedRow'][./td[1][text()='CLIENTNAME_REPLACE']]");
	By by_Row_DesiredClient;
	public WebElement getDesiredClient(String ClientName) throws Exception{
		getTextBoxFilter().clear();
		
		by_Row_DesiredClient = new By.ByXPath(xpath_Row_DesiredClient.replace("CLIENTNAME_REPLACE", ClientName));
		WebElement we = ReturnSubWebElement("Row : Client Name - " + ClientName, getTableOfRecords(), by_Row_DesiredClient, GlobalShortWait, GlobalShortRetry);
		if(we==null){
			return null;
		}
		MoveToElement(we);
		return we;
	}
	
	//Harish
		public void getMyClient(String ClientName) throws Exception{
			
			Enteringusername().clear();
			Enteringusername().sendKeys(ClientName);
			Thread.sleep(2000);
			if(selectusername(ClientName)!=null){
				selectusername(ClientName).click();
				Log.info("User "+ClientName+" is available to Edit Timesheet");
			}else{
				Log.warn("User "+ClientName+" is not available to Edit Timesheet");
			}
		}
		
		By by_Entering_username = new ByXPath("//*[@id='listFilter']");
		public WebElement Enteringusername(){
			
			return driver.findElement(by_Entering_username);
		}
		String username=("//*[text()='CLIENTNAME_REPLACE']");
		public WebElement selectusername(String ClientName) throws Exception{
			By by_select_username= new ByXPath(username.replace("CLIENTNAME_REPLACE",ClientName));
			return ReturnWebElement("User Name",by_select_username,GlobalShortWait, GlobalShortRetry, null);
		}
		By by_Select_clientname = new ByXPath("//*[@name='project.customer']");
		public WebElement SelctClientname(){

			return driver.findElement(by_Select_clientname);
		}
		public boolean CheckClientname(String ClientName) throws Exception{
			try {
				Select oSelect = new Select(SelctClientname());
				if(oSelect.getOptions().contains(ClientName)) {
					Log.info("The created "+ClientName+" is displayed while assigning a project to the user.");
					return true;
				} else {
					Log.info("The created "+ClientName+" is not displayed while assigning a project to the user.");
					return false;
				}
			} catch (Exception E) {
				System.out.println("The test case is failed with exception : "+E);
				return false;
			}

		}
		By by_Export_to_Excel = new ByXPath("//a[@title='Export to Excel']");
		public WebElement ExcelExportElement(){

			return driver.findElement(by_Export_to_Excel);
		}
		public boolean ExportExcel(String Client) throws Exception {
			try {
				ExcelExportElement().click();
				Thread.sleep(9000);
				String home = System.getProperty("user.home");
				String folder = home+"/Downloads/out.xlsx";
				XSSFWorkbook workbook = new XSSFWorkbook(folder);
				XSSFSheet sheet = workbook.getSheet("Export");
				int totalNoOfRws = sheet.getPhysicalNumberOfRows();
				for(int i=0;i<=totalNoOfRws;i++) {
					if(workbook.getSheetAt(0).getRow(i).getCell(0).toString().equals(Client)){
						System.out.println(""+Client+"");
						workbook.close();
						File file = new File(folder);
						if(file.delete()){
							Log.info(file.getName() + " is deleted!");
						}else{
							Log.fatal("Delete operation is failed.");
						}
						return true;
					} else if(workbook.getSheetAt(0).getRow(i).getCell(0).toString().isEmpty()) {
						System.out.println("The Cell is empty our required client name "+Client+" is not displayed");
						File file = new File(folder);
						if(file.delete()){
							Log.info(file.getName() + " is deleted!");
						}else{
							Log.fatal("Delete operation is failed.");
						}
						return false;
					} else {
//						System.out.println(workbook.getSheetAt(0).getRow(i).getCell(0));
					}
				}
				File file = new File(folder);
				if(file.delete()){
					Log.info(file.getName() + " is deleted!");
				}else{
					Log.fatal("Delete operation is failed.");
				}
				return false;
			} catch (Exception E) {
				return false;
			}
		}
	By by_Active_status = new ByXPath("//*[@id='active']");
	public WebElement ActiveClientStatus(){

		return driver.findElement(by_Active_status);
	}
		public boolean MakeInactive() {
			if(ActiveClientStatus().isSelected()) {
				Log.info("The Client is in active state , now it will be made inactive");
				ActiveClientStatus().click();
				getButtonSave().click();
				return true;
			} else {
				Log.info("The Client is already in inactive state , now it will be made inactive");
				return false;
			}
		}
		public boolean SearchInactiveCleint(String client) throws InterruptedException {
			Enteringusername().clear();
			Enteringusername().sendKeys(client);
			Thread.sleep(1000);
			try {
				if(driver.findElement(By.xpath("//tr[@class='filterRow']/td[1][contains(text(),'"+client+"')]")).isDisplayed()) {
					Log.info("The client named "+client+" is displayed after making inactive.");
					return false;
				} else {
					Log.info("The client named "+client+" is not displayed as it is made inactive");
					return true;
				}
			} catch(NoSuchElementException E) {
				Log.info("The inactive client is not displayed after the search "+E);
				return true;
			} catch (Exception E) {
				Log.fatal("The method SearchInactiveCleint is failed with exception " +E);
				return false;
			}
		}
	By by_make_cleint_active = new ByXPath("//span[@title='Only active clients?']");
	public WebElement DisplayInactiveClients(){

		return driver.findElement(by_make_cleint_active);
	}
	public boolean MakeClientActive(String client) throws InterruptedException {

		try {
			Enteringusername().clear();
			DisplayInactiveClients().click();
			Thread.sleep(2000);
			System.out.println("qqqqqqqqqqqqqqqq");
			Enteringusername().clear();
			Enteringusername().sendKeys(client);
			Thread.sleep(1000);
			if(driver.findElement(By.xpath("//tr[@class='filterRow']/td[1][contains(text(),'"+client+"')]")).isDisplayed()) {
				driver.findElement(By.xpath("//tr[@class='filterRow']/td[1][contains(text(),'"+client+"')]")).click();
				ActiveClientStatus().click();
				getButtonSave().click();
				Thread.sleep(2000);
				Log.info("The client named "+client+" is displayed after making active and he is made.");
				return true;
			} else {
				Log.info("The client named "+client+" is not displayed as it is made active");
				return false;
			}
		} catch(Exception e) {
			Log.error("Failed to verify the client status active/inactive");
			return false;
		}
	}
		//Harish
		
	public WebElement getTableClientList() throws Exception{
		return ReturnWebElement("Table - Client List", by_Table_ClientList, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public WebElement getSelectDisplayedRowByClientName(String UserName) throws Exception{
		xpath_ClientList_SelectRowByClientName = xpath_ClientList_SelectRowByClientName.replace("REPLACE", UserName);
		by_Table_ClientList_SelectRowByClientName = new By.ByXPath(xpath_ClientList_SelectRowByClientName);
		return ReturnSubWebElement("", getTableClientList(), by_Table_ClientList_SelectRowByClientName, GlobalShortWait, GlobalShortRetry);
	}
	
	public WebElement getTextBoxClientName() throws Exception{
		return ReturnWebElement("Text Box - Client Name", by_TextBox_CustomerName, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public WebElement getTextBoxClientCode() throws Exception{
		return ReturnWebElement("Text Box - Client Code", by_TextBox__CustomerCode, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public WebElement getTextBoxClientDescription() throws Exception{
		return ReturnWebElement("Text Box - Client Description", by_TextBox_CustomerDescription,GlobalShortWait, GlobalShortRetry, null);
	}

	
	public WebElement getCheckBoxClientActive() throws Exception{
		return ReturnWebElement("CheckBox - Is Client Active", by_CheckBox_CustomerActive,GlobalShortWait, GlobalShortRetry, null);
	}

	public WebElement getError(){
		return driver.findElement(by_Error);
	}	
	
	public WebElement getTextBoxFilter() throws Exception{
		return ReturnWebElement("TextBox - Filter", by_TextBox_Filter, GlobalShortWait, GlobalShortRetry, null);
	}
	
	public Integer getDisplayedFilteredClientCount() throws Exception{
		List<WebElement> we = ReturnSubWebElements("Filtered Client Rows", by_Table_ClientList,by_Table_ClientList_DisplayedRows, GlobalShortWait, GlobalShortRetry); 
		return (we == null?0:we.size());
	}


	By by_Button_Save = new By.ByName("submitButton");
	public WebElement getButtonSave(){
		return driver.findElement(by_Button_Save);
	}
	
	public void CreateClient(DataTable table) throws Exception{
		String ClientName,ClientCode,ClientDescription;
		Boolean IsActive;
		ClientName= table.raw().get(1).get(0);
		ClientCode = table.raw().get(1).get(1);
		ClientDescription = table.raw().get(1).get(2);
		IsActive = table.raw().get(1).get(3).equals("true")?true:false;
		
		getTextBoxClientName().sendKeys(ClientName);
		getTextBoxClientCode().sendKeys(ClientCode);
		getTextBoxClientDescription().sendKeys(ClientDescription);
		CheckOrUncheckTheCheckBox(IsActive, getCheckBoxClientActive());
		getButtonSave().click();
		AssertTrue("Data Saved Message", "Data saved", (getError().getText()!=null ||getError().getText().length()>0)?getError().getText():"<Blank Value>" , getError().getText().equals("Data saved"));
		//Assert.assertTrue(getError().getText().equals("Data saved"));
	}
	
	
	public void SearchClients(String ClientName) throws Exception{
		getTextBoxFilter().clear();
		getTextBoxFilter().sendKeys(ClientName);
		
	}
	

	By by_Button_Delete = new By.ByXPath("//a[@class='button'][./span[text()='Delete']]");
	public WebElement getButtonDelete() throws Exception{
		return ReturnWebElement("Button Delete", by_Button_Delete, GlobalShortWait, GlobalShortRetry, null);
	}
	
	

//	public Alert getDeleteAlert(){
//		Alert a = (new WebDriverWait(driver, 3).until(ExpectedConditions.alertIsPresent()));
//		System.out.println( a.getText());
//		return a;
//	}
	
	public void DeleteClient(String ClientName) throws Exception{
		getButtonDelete().click();
		getDeleteAlert().accept();
		AssertTrue("Data Saved Message", "Data saved", (getError().getText()!=null ||getError().getText().length()>0)?getError().getText():"<Blank Value>" , getError().getText().equals("Data saved"));
	}
	
	public void VerifyIfClientRecordCanBeFoundInSearchResults(String ClientName) throws Exception{
		SearchClients(ClientName);
		AssertTrue("Client Count", 1, getDisplayedFilteredClientCount()!=null?getDisplayedFilteredClientCount():"null", getDisplayedFilteredClientCount().equals(1));
		//Assert.assertTrue("The client count is not 1",getDisplayedFilteredClientCount().equals(1));
	}
	
	public void SelectClientRowFromSearchResults(String ClientName) throws Exception{
		getSelectDisplayedRowByClientName(ClientName).click();
		//Thread.sleep(3000);
		AssertTrue("TextBox - Client Name", ClientName, GetTextBoxValue(getTextBoxClientName()), GetTextBoxValue(getTextBoxClientName()).equals(ClientName));
		//Assert.assertTrue("The client value is " + GetTextBoxValue(getTextBoxClientName()) + " instead of " + ClientName , GetTextBoxValue(getTextBoxClientName()).equals(ClientName));
	}
	
	public void VerifySelectedClient(DataTable table) throws Exception{
		String ClientName,ClientCode,ClientDescription;
		Boolean IsActive;
		ClientName= table.raw().get(1).get(0);
		ClientCode = table.raw().get(1).get(1);
		ClientDescription = table.raw().get(1).get(2);
		IsActive = table.raw().get(1).get(3).equals("true")?true:false;
		
		AssertTrue("TextBox - Client Name", ClientName, GetTextBoxValue(getTextBoxClientName()), GetTextBoxValue(getTextBoxClientName()).equals(ClientName));
		//Assert.assertTrue(GetTextBoxValue(getTextBoxClientName()).equals(ClientName));
		
		AssertTrue("TextBox - Client Code", ClientCode, GetTextBoxValue(getTextBoxClientCode()), GetTextBoxValue(getTextBoxClientCode()).equals(ClientCode));
		//Assert.assertTrue(GetTextBoxValue(getTextBoxClientCode()).equals(ClientCode));
		
		AssertTrue("TextBox - Client Description", ClientDescription, GetTextBoxValue(getTextBoxClientDescription()), GetTextBoxValue(getTextBoxClientDescription()).equals(ClientDescription));
		//Assert.assertTrue(GetTextBoxValue(getTextBoxClientDescription()).equals(ClientDescription));
		
		
		AssertTrue("CheckBox - Active",IsActive,IsCheckBoxChecked(getCheckBoxClientActive()),IsCheckBoxChecked(getCheckBoxClientActive())==IsActive);
		//Assert.assertTrue(IsCheckBoxChecked(getCheckBoxClientActive())==IsActive);
	
	}
}
