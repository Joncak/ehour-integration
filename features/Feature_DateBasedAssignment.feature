@FeatureDateBasedAssignment
Feature: Assigning a date based project to the user

@DateBasedAssignment
Scenario: Create Users and assign them to project I want to create a user, assign a date based project to the user and verify if the same is reflecting on the user account
    Given Open browser and start application
    When I enter valid "admin" and "admin"
    Then I should be able see "Admin, eHour" on Welcome Page
    When I select menu item "Users"
    Then Page Header must display "User management"
    When I add a new user with the following details if non existent
	  | Username   | FirstName | LastName | Email             | Password  | ConfirmPassword | Department | UserRole | Active | AssignToProjectAfterSave | Country  | Location | EmployeeType |TimeSheetApprover|
	 	| JamesCam   | Jamesc     | Cameron  | Jamecam@nisum.com | yahoo$123 | yahoo$123       | Offshore   | User     | true   | false                   | Pakistan | Karachi  | Nisum        |Admin, eHour		 |
    When I select menu item "Assignments"
    When I select a client named "Cameron"
    Then I should assign a project with Start and End date
      | Assignment type |
      | Date range      |
      
    Given Open browser and start application
    When I enter valid "JamesCam" and "yahoo$123"
		Then I should be able see "Cameron, Jamesc" on Welcome Page
    Then i check if project is displayed or not when the user is logged in
    Then i check if project is not displayed when i select the other dates in time frame
    When I enter valid "admin" and "admin"
    Then I should be able see "Admin, eHour" on Welcome Page
    When I select menu item "Users"
	  Then Page Header must display "User management"
	  When I Select the user with username - "Cameron"
    When I press the delete button
    Then I cannot search the user with the username - "Cameron"	
		When I logout from eHour
		Then I am successfully logged out
