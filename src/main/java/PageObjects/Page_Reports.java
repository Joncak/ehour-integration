package PageObjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.DataTable;
import stepDefinition.BaseSteps;

import java.util.ArrayList;
import java.util.List;

import static PageObjects.Base.*;

public class Page_Reports {

    By by_Date_ReportFrom = new By.ByXPath("//input[@name='dateStart']");
    By by_Date_ReportTo = new By.ByXPath("//input[@name='dateEnd']");
    By by_Select_Week = new By.ByXPath("//select[@name='quickWeek']");
    By by_Select_Month = new By.ByXPath("//select[@name='quickMonth']");
    By by_Select_Quarter = new By.ByXPath("//select[@name='quickQuarter']");

    By by_Select_Clients = new By.ByXPath(".//*[@id='customerSelect']");
    By by_Select_Users = new By.ByXPath(".//*[@id='userSelect']");
    By by_Select_ClientBCI = new By.ByXPath("//select[@id='customerSelect']/option[2]");
    By by_CheckBox_Clients_Active = new By.ByXPath("//input[@type='checkbox'][@name='customerProjectsBorder:customerProjectsBorder_body:reportCriteria.userSelectedCriteria.onlyActiveCustomers']");

    //WebElement by_Select_Projects = driver.findElement(By.xpath("//select[@id='projectSelect']"));
    By by_Select_Projects = new By.ByXPath("//select[@id='projectSelect']");
    By by_CheckBox_Projects_Active = new By.ByXPath("//input[@type='checkbox'][@name='customerProjectsBorder:customerProjectsBorder_body:reportCriteria.userSelectedCriteria.onlyActiveProjects']");
    By by_CheckBox_Projects_Billable = new By.ByXPath("//input[@type='checkbox'][@name='customerProjectsBorder:customerProjectsBorder_body:reportCriteria.userSelectedCriteria.onlyBillableProjects']");

    By by_Select_FunctionalGroup = new By.ByXPath("//select[@id='departmentSelect']");

    By by_Select_User = new By.ByXPath("//select[@id='userSelect']");
    By by_CheckBox_User_Active = new By.ByXPath("//input[@type='checkbox'][@name='userDepartmentPlaceholder:deptUserBorder:deptUserBorder_body:reportCriteria.userSelectedCriteria.onlyActiveUsers']");
    By by_CheckBox_User_ContractorsOnly = new By.ByXPath("//input[@type='checkbox'][@name='userDepartmentPlaceholder:deptUserBorder:deptUserBorder_body:reportCriteria.userSelectedCriteria.onlyContractors']");

    By by_MenuItem_Reporting = new By.ByXPath("//a[text()='Reporting']");
    By by_Tab_GlobalReporting = new By.ByXPath("//span[text()='Global report']");
    By by_Button_CreateReport = new By.ByXPath("//span[text()='Create report']");
    By by_Tab_ClientReporting = new By.ByXPath("//span[text()='Client report']");
    By by_Date_ReportPeriod = new By.ByXPath("//div[@class='criteriaDate']");
    By by_Client_ReportPage = new By.ByXPath("//span[@id='id44']/table/tbody/tr[1]/td[1]");
    By by_Projects_ReportPage = new By.ByXPath("//span[@id='id480']/table/tbody/tr/td[2]");

    String storeClient;
    List<String> projectslist = new ArrayList<>();
    public List<String> clients = new ArrayList<String>();


    public WebElement getMenuReporting() throws Exception {
        return ReturnWebElement("MenuItem-Reporting", by_MenuItem_Reporting, GlobalShortWait, GlobalShortRetry, null);

    }


    public String getTabGlobalReport() throws Exception {
        return ReturnWebElement("TabGlobalReport", by_Tab_GlobalReporting, GlobalShortRetry, GlobalShortWait, null).getText();

    }

    public String getTabClientReport() throws Exception {
        return ReturnWebElement("TabClientReport", by_Tab_ClientReporting, GlobalShortRetry, GlobalShortWait, null).getText();

    }

    public String getReportPeriod() throws Exception {
        return ReturnWebElement("Report Period", by_Date_ReportPeriod, GlobalShortRetry, GlobalShortWait, null).getText();

    }

    public WebElement getStartDate() throws Exception {
        return ReturnWebElement("Date ReportFrom", by_Date_ReportFrom, GlobalShortWait, GlobalShortRetry, null);
    }

    public WebElement getEndDate() throws Exception {
        return ReturnWebElement("Date ReportTo", by_Date_ReportTo, GlobalShortWait, GlobalShortRetry, null);
    }

    public void getStartDateEndDate(String startdate, String enddate) throws Exception {

        getStartDate().clear();
        getStartDate().sendKeys(startdate);
        getEndDate().clear();
        getEndDate().sendKeys(enddate);

    }


    public WebElement getClients() throws Exception {
        return ReturnWebElement("Select ClientBCI", by_Select_Clients, GlobalShortWait, GlobalShortRetry, null);
    }

    public void selectClients(List<String> listofclients) throws Exception {
    	for(int i=0;i<listofclients.size();i++){
    		Select oselect= new Select(getClients());
            oselect.selectByVisibleText(listofclients.get(i));
    	}
	
    }
    
    public WebElement getUsers() throws Exception {
        return ReturnWebElement("Select Users", by_Select_Users, GlobalShortWait, GlobalShortRetry, null);
    }
    
    public void selectSpecificUsers(List<String> listofusers) throws Exception{
    	for(int i=0;i<listofusers.size();i++){
    		Select oselect= new Select(getUsers());
            oselect.selectByVisibleText(listofusers.get(i));
    	}
    }
    
    public void listOfClients(DataTable dt){
    	
    	for(int i=1;i<dt.raw().size();i++){
    		clients.add(dt.raw().get(i).get(0));
    	}
    }

    public WebElement getListOfProjects() throws Exception {

        return ReturnWebElement("Select Projects", by_Select_Projects, GlobalShortWait, GlobalShortRetry, null);
    }

    public void storeAllProjects() throws Exception {

        Select projects = new Select(getListOfProjects());


        List<WebElement> options = projects.getOptions();
        System.out.println("#####" + options.size());

        for (int i = 0; i < options.size(); i++)

        {
            String fullProjectName = options.get(i).getText();
            projectslist.add(fullProjectName.substring(fullProjectName.lastIndexOf("-") + 1).trim());
            System.out.println("Option Value : " + projectslist.get(i));

        }

    }

    public WebElement getCreateReport() throws Exception {

        return ReturnWebElement("Click CreateReport", by_Button_CreateReport, GlobalShortWait, GlobalShortRetry, null);

    }

    public void clickReport() throws Exception {
        getCreateReport().click();

    }

    public String getVisibleClient() throws Exception {

        return ReturnWebElement("Get VisibleClient", by_Client_ReportPage, GlobalShortWait, GlobalShortRetry, null).getText();

    }


    public void compareProjectsGlobalReportAndClientReport() throws Exception {
        System.out.println(driver.getPageSource());
        for (String projectOnGlobal : projectslist) {
            Assert.assertTrue("message", driver.getPageSource().contains(projectOnGlobal));
        }
    }
    
    public void getValuesofthereport(DataTable dt){
    	 
    	//First loop will find the Client Name in the first column
    			for (int i=1;i<=5;i++){
    						// If the client from report match with the description of client from table , it will initiate one more inner loop for all the columns of 'i' row 
    						for (int j=1;j<=14;j++){
    							String valuefromreport= driver.findElement(By.xpath(".//table/tbody/tr[" + i + "]/td[" + j + "]")).getText();
    							String valuefromtable=dt.raw().get(i).get(j-1);
    							BaseSteps.AssertTrue("Table Value Verification", valuefromtable, valuefromreport, valuefromtable.equalsIgnoreCase(valuefromreport));	
    					}
    					
    				Log.info("Validation for "+i+" row is completed ");
    				System.out.println("########################################################################");
    				}
    }
    
    public void validatehoursandturnover(String hours,String turnover){
    	String hoursfromreport=driver.findElement(By.xpath(".//table/tfoot/tr/td[13]")).getText();
    	String turnoverfromreport=driver.findElement(By.xpath(".//table/tfoot/tr/td[14]")).getText();
    	BaseSteps.AssertTrue("Total Hours Verification", hours, hoursfromreport, hours.equalsIgnoreCase(hoursfromreport));
    	BaseSteps.AssertTrue("Total turn over Verification", turnover, turnoverfromreport, turnover.equalsIgnoreCase(turnoverfromreport));
    	Log.info("Total Hours and Turn Over are Matching in the Report");
    	
    }
}
