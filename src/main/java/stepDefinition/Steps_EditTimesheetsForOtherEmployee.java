package stepDefinition;

import PageObjects.Page_Client;
import PageObjects.Page_Header;
import PageObjects.Page_Login;
import PageObjects.Page_Timesheet;
import PageObjects.Page_UserManagement;
import cucumber.api.DataTable;
import cucumber.api.java.en.*;

public class Steps_EditTimesheetsForOtherEmployee {

	
	Page_Timesheet pd = new Page_Timesheet(null);
	Page_Client pc = new Page_Client("Anything");
	Page_Header h = new Page_Header("anything");
	Page_UserManagement pum;
	Page_Login lp = new Page_Login(null);
	
	@Then("^I select a user \"([^\"]*)\" to edit the timesheet$")
	public void i_select_a_user_to_edit_the_timesheet(String username) throws Throwable {
		String ClientName=null;
		if(username.equalsIgnoreCase("user1")){
			ClientName=pum.unique_lastname_u1;
		}else if(username.equalsIgnoreCase("user2")){
			ClientName=pum.unique_lastname_u2;
		}else{
			ClientName=username;
		}
		pc.getMyClient(ClientName);
		
		
	}
	@Then("^I click on Temporarily sign in and check if i logged in temporarily to that user i should see \"([^\"]*)\"$")
	public void i_click_on_Temporarily_sign_in_and_check_if_i_logged_in_temporarily_to_that_user_i_should_see(String Name) throws Throwable {
		if(pd.TemporarySignIn()) {
			BaseSteps.AssertTrue("User Name on Welcome Page",Name, h.getLoggedInAs(),h.getLoggedInAs().equals(Name));
		}
		
	}
	
	@Then("^I click on Temporarily sign in and check if i logged in temporarily to the \"([^\"]*)\"$")
	public void i_click_on_Temporarily_sign_in_and_check_if_i_logged_in_temporarily_to_the_user(String uname) throws Throwable {
		String Name=null;
		if(uname.equalsIgnoreCase("user1")){
			Name=pum.unique_lastname_u1+", "+pum.unique_firstname_u1;
			if(pd.TemporarySignIn()) {
				//lp.handleModelPopUp();
				BaseSteps.AssertTrue("User Name on Welcome Page",Name, h.getLoggedInAs(),h.getLoggedInAs().equals(Name));
			}
		}
	}
	
	@And("^I update the work location as \"([^\"]*)\"$")
	public void i_update_work_location_as(String location) throws Throwable{
		pd.selectWorkLocation(location);
	}
	

	@Then("^I fill the timesheet of the user as the following$")
	public void i_fill_the_timesheet_of_the_user_as_the_following(DataTable bc) throws Throwable {

		pd.EditTimeSheetData(bc);
	}
	
	@Then("^I enter a comment for that week and click on save$")
	public void i_click_on_save_enter_some_comments() throws Throwable {
		pd.SavingTimesheet();
	}
	
	@Then("^I switch back to the admin account$")
	public void i_switch_back_to_the_admin_account() throws Throwable {

		pd.SwitchingBackToAdmin();
		
	}

	@Then("^I check if the timesheets are filled properly by admin or not$")
	public void i_check_if_the_timesheets_are_filled_properly_by_admin_or_not() throws Throwable {
		pd.VerifyingFilledHours();
	}
	
	@Then("^I check if the timesheets are filled properly by admin is \"([^\"]*)\" hours and for location \"([^\"]*)\"$")
	public void i_check_if_the_timesheets_are_filled_properly(String hours, String loc) throws Throwable {
		pd.VerifyFilledHours(hours,loc);
	}
	
	@And("^I verify if default work location \"([^\"]*)\" is displayed in the week where timesheet is not filled or not stored$")
	public void i_verify_if_default_work_location_is_displayed(String defaultlocation) throws Exception{
		pd.verifydefaultlocation(defaultlocation);
	}
	
	
	@Then("^I remove the filled timesheet again to delete the user and click on save$")
	public void i_remove_the_filled_timesheet_again_to_delete_the_user_and_click_on_save(DataTable empty) throws Throwable {
		pd.ClearTimeSheetDataTable(empty);
		//pd.ClearTimeSheetData(empty);
		//pd.clearingPTOMondayEntry();
		pd.EmptyingTimesheet();
	}

}
