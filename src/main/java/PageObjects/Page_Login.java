package PageObjects;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.remote.adapter.RemoteResultListener;

public class Page_Login extends Base{

	ArrayList<String> tabs;


	
	public Page_Login(String BrowserType) {
		super(BrowserType);
	}
	
	public void NavigateTo() throws IOException{
		String BaseURL = FetchPropertyValues("URL_Base");
		String LoginURI = FetchPropertyValues("URI_LoginPage");
		driver.get(BaseURL + LoginURI);
	}

	
	public WebElement getUserName(){
		return driver.findElement(By.name("username"));
	}
	
	public WebElement getPassword(){
		return driver.findElement(By.name("password"));
	}
	
	public WebElement getSubmit(){
		return driver.findElement(By.id("loginSubmit"));
	}
	
	
	
	public void Login(String UserName, String Password) throws InterruptedException{
		getUserName().clear();
		getUserName().sendKeys(UserName);
		getPassword().clear();
		getPassword().sendKeys(Password);
		getSubmit().click();
	}

	public void NavigateToGoogle() throws IOException{
		driver.manage().deleteAllCookies();
		String GoogleURL = FetchPropertyValues("URL_Google");
		driver.get(GoogleURL);
	}
	public void GetTabsCount(){

		((JavascriptExecutor)driver).executeScript("window.open()");
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
	}
	
	By by_popup_close = new By.ByXPath(".//*[@id='_wicket_window_2']/a");
	public void handleModelPopUp() throws Exception{
		WebElement popclose=ReturnWebElement("Modal Popup Close Button", by_popup_close, GlobalShortWait, GlobalShortRetry, null);
		if(popclose!=null){
			popclose.click();
		}else{
			System.out.println("No Modle Popup Displayed");
		}
	}
	
	//SSO implementation begins
	By by_Input_NisumUser = new By.ById("identifierId");
	By by_Button_Next = new By.ByXPath("//span[text()='Next']");
	By by_Input_NisumPassword = new By.ByName("password");
	By by_Button_GooglePlus = new By.ByXPath("//div[@class='google-signIn']/a/i");
	By by_Email;
	By by_Button_Compose = new By.ByXPath("//div[text()='COMPOSE']");
	By by_Link_SignIn = new By.ByXPath("//a[text()='Sign in']");
	By by_Error = new By.ByXPath("//span[@class='errorlevel feedbackPanelERROR']");
	//By by_Link_SSO_GoogleAccount = new By.ByXPath("//span[@class='gb_8a gbii']");
	By by_Link_SSO_GoogleAccount = new By.ByCssSelector("span[class$='gbii']");
	//By by_Link_SSO_SignOut = new By.ByXPath("//a[@id='gb_71']");
	By by_Link_SSO_SignOut = new By.ByCssSelector("a[id^='gb_']");

	public WebElement getInputNisumUser() throws Exception{
		return ReturnWebElement("Nisum User", by_Input_NisumUser, GlobalShortWait, GlobalShortRetry, null); // driver.findElement(By.id("identifierId"));
	}
	

	public WebElement getButtonNext() throws Exception { 
		return ReturnWebElement("Next Button", by_Button_Next, GlobalShortWait, GlobalShortRetry, null);
	}

	public WebElement getInputNisumPwd() throws Exception 
	{
		return ReturnWebElement("Password", by_Input_NisumPassword, GlobalShortWait, GlobalShortRetry, null);
	}

	//public WebElement pwdNext() {return driver.findElement(By.xpath("//span[text()='Next']"));}

	public WebElement getButtonGooglePlus() throws Exception{
		return ReturnWebElement("Button - Google Plus", by_Button_GooglePlus, GlobalShortWait, GlobalShortRetry,null);
	}

	String xpath_Email = "//div[./div/p[text()='REPLACE_EMAIL']]";
	
	public WebElement getLabelEmail(String Email) throws Exception{
		by_Email = new By.ByXPath(xpath_Email.replace("REPLACE_EMAIL", Email));
		return ReturnWebElement("User Email",by_Email,GlobalShortWait, GlobalShortRetry,null);
	}

	public WebElement getButtonCompose() throws Exception{
		return ReturnWebElement("Compose", by_Button_Compose,GlobalShortWait,GlobalShortRetry, null);
	}

	//	public WebElement  getSignout(){return driver.findElement(By.xpath("//div[@class='LoggedInAs']/a[2])"));}

	public WebElement getButtonSignIn() throws Exception{
		return ReturnWebElement("Sign In", by_Link_SignIn,GlobalShortWait, GlobalShortRetry, null);
		
	}

//	public WebElement getError(){return driver.findElement(By.xpath("//span[text()='incorrect username and password combination']"));}
	
	String xpath_SignedInGoogle = "//a[contains(@title,'REPLACE_EMAIL')]";
	
	By by_Link_SignedInGoogle;  
	public WebElement getLinkSignedInGoogle(String Email) throws Exception{
		by_Link_SignedInGoogle = new By.ByXPath(xpath_SignedInGoogle.replace("REPLACE_EMAIL", Email));
		return ReturnWebElement("Signed In as Google User Name" + Email, by_Link_SignedInGoogle,GlobalShortWait,GlobalShortRetry,null);
	}
	
	public void SignInToGoogle(String UN, String Pwd ) throws Exception {
		getButtonSignIn().click();
		
		if (getInputNisumUser() == null && getInputNisumPwd()!=null){
			getInputNisumPwd().sendKeys(Pwd);
			getButtonNext().click();
		}else{
			getInputNisumUser().sendKeys(UN);
			getButtonNext().click();
			getInputNisumPwd().sendKeys(Pwd);
			getButtonNext().click();
		}
		AssertTrue("Signed in Google as Username" + UN, true , getLinkSignedInGoogle(UN).isDisplayed(), getLinkSignedInGoogle(UN).isDisplayed() == true);
	}
	
	public void SSOLogin(String Email) throws Exception{
		getButtonGooglePlus().click();
		SwitchToWindow("Sign in - Google Accounts");
		getLabelEmail(Email).click();
		Thread.sleep(5000);
		SwitchToWindow("eHour - month overview");
	}

	
	public WebElement getErrorMessage() throws Exception{
		//Thread.sleep(5000);
		return ReturnWebElement("Error", by_Error, GlobalShortWait,GlobalShortRetry,null);
	}

	public WebElement getSSOGoogleAccount() throws Exception {
		return ReturnWebElement("Link - SSO Google Account", by_Link_SSO_GoogleAccount, GlobalShortWait, GlobalShortRetry, null);

	}

	public WebElement getSSOSignOut() throws Exception {
		return ReturnWebElement("Link - SSO Sign Out", by_Link_SSO_SignOut, GlobalShortWait, GlobalShortRetry, null);

	}

	public void SSOSignOut() throws Exception{
		getSSOGoogleAccount().click();
		getSSOSignOut().click();
	}



	public void CloseDriver(){
		driver.switchTo().window(tabs.get(0)).close();
	}
	
}
