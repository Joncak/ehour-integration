package stepDefinition;

import PageObjects.Log;
import PageObjects.Page_Assignments;
import PageObjects.Page_UserManagement;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps_Assignments {

	Page_Assignments as = new Page_Assignments(null);
	Page_UserManagement pum =  new Page_UserManagement(null) ;
	
	
	@When("^I assign projects to users as follows$")
	public void i_assign_projects_to_users_as_follows(DataTable dt) throws Throwable {
		for(int i=1;i<dt.raw().size();i++){
			as.getDesiredUser(dt.raw().get(i).get(0), dt.raw().get(i).get(1)).click();
			//Thread.sleep(3000);
			as.AssignProject(dt.raw().get(i));
			//Thread.sleep(3000);
		}
	}
	@When("^I assign projects to users as follows date based$")
	public void i_assign_projects_to_users_as_follows_date_based(DataTable dt) throws Throwable {
		for(int i=1;i<dt.raw().size();i++){
			as.getDesiredUser(dt.raw().get(i).get(0), dt.raw().get(i).get(1)).click();
			//Thread.sleep(3000);
			as.AssignDateBasedProject(dt.raw().get(i));
			//Thread.sleep(3000);
		}
	}

	@When("^I assign projects to users as follows with dates$")
	public void i_assign_projects_to_users_as_follows_with_dates(DataTable dt) throws Throwable {
		for(int i=1;i<dt.raw().size();i++){
			as.getDesiredUser(dt.raw().get(i).get(0), dt.raw().get(i).get(1)).click();
			//Thread.sleep(3000);
			as.AssignProjectWithDates(dt.raw().get(i));
			//Thread.sleep(3000);
		}
	}

	@When("^I assign projects to \"([^\"]*)\" as follows$")
	public void i_assign_projects_to_as_follows(String uname, DataTable dt) throws Throwable {
	    if(uname.equalsIgnoreCase("user1")){
	    	for(int i=1;i<dt.raw().size();i++){
				as.getDesiredUser(pum.unique_firstname_u1, pum.unique_lastname_u1).click();
				as.AssignProject(dt.raw().get(i));
			}
	    }else{
	    	for(int i=1;i<dt.raw().size();i++){
				as.getDesiredUser(pum.unique_firstname_u2, pum.unique_lastname_u2).click();
				as.AssignProject(dt.raw().get(i));
			}
	    }
	    Log.info("Assigned Project to "+uname);
	}
	
	
	@When("^I unassign projects to users as follows$")
	public void i_unassign_projects_to_users_as_follows(DataTable dt) throws Throwable {
		for(int i=1;i<dt.raw().size();i++){
			//Thread.sleep(2000);
			as.getDesiredUser(dt.raw().get(i).get(0), dt.raw().get(i).get(1)).click();
			//Thread.sleep(2000);
			as.DeleteProjectAssignment(dt.raw().get(i).get(2), dt.raw().get(i).get(3));
		}
	}
	
	@When("^I unassign projects to \"([^\"]*)\" as follows$")
	public void i_unassign_projects_to_as_follows(String uname, DataTable dt) throws Throwable {
		if(uname.equalsIgnoreCase("user1")){
			for(int i=1;i<dt.raw().size();i++){
				as.getDesiredUser(pum.unique_firstname_u1, pum.unique_lastname_u1).click();
				as.DeleteProjectAssignment(dt.raw().get(i).get(2), dt.raw().get(i).get(3));
			}
		}else{
			for(int i=1;i<dt.raw().size();i++){
				as.getDesiredUser(pum.unique_firstname_u2, pum.unique_lastname_u2).click();
				as.DeleteProjectAssignment(dt.raw().get(i).get(2), dt.raw().get(i).get(3));
			}
		}
		Log.info("Unassigned the projects for "+uname);
	}
	
	@When("^I edit assignments as follows$")
	public void i_edit_assignments_as_follows(DataTable dt) throws Throwable {
		for(int i=1;i<dt.raw().size();i++){
			//Thread.sleep(3000);
			as.getDesiredUser(dt.raw().get(i).get(0), dt.raw().get(i).get(1)).click();
			//Thread.sleep(3000);
			as.EditProjectAssignment(dt.raw().get(i));

		}
	}
	
	@When("^I edit assignment of \"([^\"]*)\" as follows$")
	public void i_edit_assignment_of_as_follows(String uname, DataTable dt) throws Throwable {
	    if(uname.equalsIgnoreCase("user1")){
	    	for(int i=1;i<dt.raw().size();i++){
				as.getDesiredUser(pum.unique_firstname_u1, pum.unique_lastname_u1).click();
				as.EditProjectAssignment(dt.raw().get(i));
			}
	    }else{
	    	for(int i=1;i<dt.raw().size();i++){
				as.getDesiredUser(pum.unique_firstname_u2, pum.unique_lastname_u2).click();
				as.EditProjectAssignment(dt.raw().get(i));
			}
	    }
	    Log.info("Edited Assignment of "+uname);
	}

	@Then("^I should see error message for the dates$")
	public void i_should_see_error_message_for_the_dates() throws Throwable {
		String error_date = pum.getDateError().getText();
		Log.info("date error :"+error_date);
		BaseSteps.AssertTrue("Start date can't be after end date","Start date can't be after end date", pum.getDateError().getText(),"Start date can't be after end date".equals(pum.getDateError().getText()));
		//BaseSteps.AssertTrue("Start date can't be after end date","Start date can't be after end date", error_date,"Start date can't be after end date".equals(error_date));
	}
	
}
